#ifndef OPTICAL_LISTENER_H
#define OPTICAL_LISTENER_H

#include <geometry_msgs/Point.h>
#include <sensor_msgs/PointCloud.h>
#include <stdlib.h>
#include "ros/ros.h"
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <tf/transform_listener.h>
#include <pcl_conversions/pcl_conversions.h>

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;
typedef pcl::PointXYZRGB Point;

class OpticalListener {
	private: PointCloud::Ptr pcPtr;
		 bool isPaused;
		 ros::Subscriber sub;
		 ros::Publisher pub;
		 std::string subTopic;
		 std::string pubTopic;
		 ros::NodeHandle* nodePtr;
		 std::vector<int>* thresholds;
		 std::vector<uint32_t>* colors;
		 void dataCallback(const pr2_pretouch_optical_dist::OpticalDist msg);
		 void genRandColor(int idx);

	public:
		OpticalListener(ros::NodeHandle* node, std::string side="r");
		~OpticalListener();
		void start();
		void pause();
		void stop();
		PointCloud::Ptr getPointCloud(std::string frameId="base_link");
		void setThresholds(std::vector<int>* newThresholds);
		void centerActive(bool on, int threshold=78);
		void leftBackLateralActive(bool on, int threshold=255);
		void leftFrontLateralActive(bool on, int threshold=255);
		void frontTipActive(bool on, int threshold=255);
		void rightBackLateralActive(bool on, int threshold=255);
		void rightFrontLateralActive(bool on, int threshold=255);
		
};	

#endif
