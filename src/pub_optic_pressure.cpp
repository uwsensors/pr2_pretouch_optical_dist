#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include <std_msgs/ByteMultiArray.h>
#include <string>
#include <vector>
#include <tf/transform_listener.h>
#include "pr2_pretouch_optical_dist/OpticalDist.h"

#define SET_PT 50

ros::Publisher* pubPtr;
std::vector<int>* storDataPtr;
std::vector<geometry_msgs::Point *> * storPosPtr;
std::vector<geometry_msgs::Point *> * storSPPtr;
tf::TransformListener* listenerPtr;
geometry_msgs::PointStamped* pointInPtr;
geometry_msgs::PointStamped* pointOutPtr;
std::vector<int> ts(4);
pr2_pretouch_optical_dist::OpticalDist data;
std::vector<short unsigned int> centerData(1);
std::vector<short unsigned int> leftBackLateralData(1);
std::vector<short unsigned int>  leftFrontLateralData(1);
std::vector<short unsigned int>  frontTipData(1);
std::vector<short unsigned int>  rightBackLateralData(1);
std::vector<short unsigned int>  rightFrontLateralData(1);
std::vector<std::string> linkNames(6);
int seq;

void transformPoint(int idx);

void rawDataCallback(const std_msgs::ByteMultiArray msg) {
	
	// Check time stamp
	if(msg.data[0] == ts[0] && msg.data[1] == ts[1] && msg.data[2] == ts[2] && msg.data[3] == ts[3]) {
		return;
	}
//	ROS_INFO("%d %d %d %d %d %d %d %d %d", msg.data[4], msg.data[5], msg.data[6], msg.data[7], msg.data[8], msg.data[9], msg.data[10], msg.data[11], msg.data[12]);
	if(msg.data[4] == msg.data[5]) {
		// Set timestamp
		ts[0] = msg.data[0];
		ts[1] = msg.data[1];
		ts[2] = msg.data[2];
		ts[3] = msg.data[3];
		
		geometry_msgs::Point * pointPos;
		// Update buffers that hold the most recent measurements
		for(int i = 0; i < msg.data[4]; i++) {
			if(msg.data[i+6] < 0) {
				(*storDataPtr)[i] = msg.data[i+6] + 256;
			} else {
				(*storDataPtr)[i] = msg.data[i+6];
			}
//			transformPoint(i);
//			pointPos = (*storPosPtr)[i];
//			pointPos->x = pointOutPtr->point.x;
//			pointPos->y = pointOutPtr->point.y;
//			pointPos->z = pointOutPtr->point.z;
		}

		// Prepare message that is to be published
		data.header.seq = seq;
		data.header.stamp = ros::Time::now();
		seq++;

		data.centerData = centerData;
		centerData[0] = (*storDataPtr)[0];
//		data.centerPos = *((*storPosPtr)[0]);
//		data.centerSP = *((*storSPPtr)[0]); 
		
		data.leftBackLateralData = leftBackLateralData;	
		leftBackLateralData[0] = (*storDataPtr)[1];
//		data.leftBackLateralPos = *((*storPosPtr)[1]);
//		data.leftBackLateralSP = *((*storSPPtr)[1]); 

		data.leftFrontLateralData = leftFrontLateralData;
		leftFrontLateralData[0] = (*storDataPtr)[2];
//		data.leftFrontLateralPos = *((*storPosPtr)[2]);
//		data.leftFrontLateralSP = *((*storSPPtr)[2]); 

		data.frontTipData = frontTipData;
		frontTipData[0] = (*storDataPtr)[3];
//		data.frontTipPos = *((*storPosPtr)[3]);
//		ROS_ERROR("x = %f, y = %f, z = %f", data.frontTipPos.x, data.frontTipPos.y, data.frontTipPos.z);
//		data.frontTipSP = *((*storSPPtr)[3]); 

		data.rightBackLateralData = rightBackLateralData;
		rightBackLateralData[0] = (*storDataPtr)[4];
//		data.rightBackLateralPos = *((*storPosPtr)[4]);
//		data.rightBackLateralSP = *((*storSPPtr)[4]); 

		data.rightFrontLateralData = rightFrontLateralData;
		rightFrontLateralData[0] = (*storDataPtr)[5];
//		data.rightFrontLateralPos = *((*storPosPtr)[5]);
//		data.rightFrontLateralSP = *((*storSPPtr)[5]);

		pubPtr->publish(data);

	}

}

void transformPoint(int idx) {
	pointInPtr->header.stamp = ros::Time(0);
	pointInPtr->header.seq = seq;
	pointInPtr->header.frame_id = linkNames[idx];
	pointInPtr->point.x = (*storDataPtr)[idx]/1000.0;
	pointInPtr->point.y = 0.0;
	pointInPtr->point.z = 0.0;
	try{
		listenerPtr->transformPoint ("base_link", *pointInPtr,*pointOutPtr);
	}catch(tf::TransformException ex) {
		if(seq > 10) {
			ROS_ERROR("%s", ex.what());
		}
		
		ros::Duration(1.0).sleep();
	}
}

int main(int argc, char ** argv) {

	ros::init(argc, argv, "optic_pressure_publisher");
	ros::NodeHandle node;

	ros::Duration(3.0).sleep();
	std::string side = argv[1];
	int n_sense = atoi(argv[2]);
	ROS_INFO("Started publisher for the %s gripper", argv[1]);
	std::string subTopic = "raw_pressure/";
	if(!side.compare("right")) {
		subTopic+="r_gripper_motor";
	} else if(!side.compare("left")) {
		subTopic+="l_gripper_motor";
	} else {
	       	ROS_INFO("%s is not a valid topic", argv[1]);
                ros::shutdown();
	}
	std::string pubTopic = "optical/"+side;
	std::vector<int> storData(n_sense);
	std::vector<geometry_msgs::Point*> storPos(n_sense);
	for(int i = 0; i < n_sense; i++) {
		storPos[i] = new geometry_msgs::Point();
	}
	std::vector<geometry_msgs::Point*> storSP(n_sense);
	for(int i = 0; i < n_sense; i++) {
		storSP[i] = new geometry_msgs::Point();
	}
	storDataPtr = &storData;
	storPosPtr = &storPos;
	storSPPtr = &storSP;
	seq = 0;
	ts[0] = 0;
	ts[1] = 0;
	ts[2] = 0;
	ts[3] = 0;

	geometry_msgs::PointStamped pointIn;
	pointInPtr = &pointIn;	
	geometry_msgs::PointStamped pointOut;
	pointOutPtr = &pointOut;

	tf::TransformListener listener;
	listenerPtr = &listener;
	
	std::string sideBegin(1,side[0]);
	linkNames[0] = sideBegin+"_gripper_l_finger_optical_ctr_link";
	linkNames[1] = sideBegin+"_gripper_l_finger_optical_lbl_link";
	linkNames[2] = sideBegin+"_gripper_l_finger_optical_lfl_link";
	linkNames[3] = sideBegin+"_gripper_l_finger_optical_frt_link";
	linkNames[4] = sideBegin+"_gripper_l_finger_optical_rbl_link";
	linkNames[5] = sideBegin+"_gripper_l_finger_optical_rfl_link";

	ros::Publisher pub = node.advertise<pr2_pretouch_optical_dist::OpticalDist>(pubTopic,1000);
	pubPtr = &pub;
	ros::Subscriber sub = node.subscribe(subTopic, 1000, rawDataCallback); 
	ros::spin();
	return 0;
}
