#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include "pr2_pretouch_msgs/GetProbabilisticPointCloud.h"
#include "geometry_msgs/PoseStamped.h"
#include <moveit/move_group_interface/move_group.h>
#include <tf/transform_listener.h>
#include "pr2_pretouch_optical_dist/OpticalRefine.h"
#include <pr2_pretouch_optical_dist/Gripper.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/Eigenvalues>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

int main(int argc, char** argv) {
	ros::init(argc, argv, "PCA_test");
	ros::NodeHandle node;
  
	ros::ServiceClient requestPCClient = node.serviceClient<std_srvs::Empty>("/tabletop_octomap");
	ros::ServiceClient getPcClient = node.serviceClient<pr2_pretouch_msgs::GetProbabilisticPointCloud>("/get_probabilistic_pointcloud");

	std_srvs::Empty emptySrv;
	ros::service::waitForService("/tabletop_octomap");
	if(requestPCClient.call(emptySrv)) {
		ROS_INFO("tabletop_octomap success");
	} else {
		ROS_INFO("tabletop_octomap failed");
	}

	pr2_pretouch_msgs::GetProbabilisticPointCloud pcSrv;
	ros::service::waitForService("/get_probabilistic_pointcloud");
	if(getPcClient.call(pcSrv)) {
		ROS_INFO("get_probabilistic_pointcloud success");
	} else {
		ROS_INFO("get_probabilistic_pointcloud failed");
	}

	std::string pubTopic1 = "optical_test_original";
	ros::Publisher pubOrigCloud = node.advertise<sensor_msgs::PointCloud>(pubTopic1,1000);

	std::string pubTopic2 = "optical_test";
	ros::Publisher pubCloud = node.advertise<sensor_msgs::PointCloud>(pubTopic2,1000);
	
	pcSrv.response.original_cloud.header.frame_id = "base_link";
	pcSrv.response.cloud.header.frame_id = "base_link";

//	ROS_INFO("# of points in cloud = %d", pcSrv.response.cloud.points.size());
//	ROS_INFO("# of points in original cloud = %d", pcSrv.response.original_cloud.points.size());
	pubOrigCloud.publish(pcSrv.response.original_cloud);
	pubCloud.publish(pcSrv.response.cloud);
  ROS_INFO("ABOUT TO POPULATE NEW CLOUD");
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcPtr(new pcl::PointCloud<pcl::PointXYZRGB>);
  ROS_INFO("CREATED CLOUD");
  pcPtr->height = 1;
  pcPtr->width = 0;
  ROS_INFO("SET HEIGHT AND WIDTH");
  //ROS_INFO("pcSrv.response.original_cloud.points.size() = %d", pcSrv.response.original_cloud.points.size());
  float zMax = pcSrv.response.original_cloud.points[0].z;
  float zMin = pcSrv.response.original_cloud.points[0].z;
  ROS_INFO("BEFORE: ZMAX = %f, ZMIN = %f",zMax,zMin);
  for(int i = 0; i<pcSrv.response.original_cloud.points.size(); i++) {
    pcl::PointXYZRGB point;
    point.x = pcSrv.response.original_cloud.points[i].x;
    point.y = pcSrv.response.original_cloud.points[i].y;
    point.z = 0.0;//0.01*pcSrv.response.original_cloud.points[i].z;
    pcPtr->push_back(point);
    pcPtr->width++;
    if(pcSrv.response.original_cloud.points[i].z < zMin) {
      zMin = pcSrv.response.original_cloud.points[i].z;
    }else if(pcSrv.response.original_cloud.points[i].z > zMax) {
      zMax = pcSrv.response.original_cloud.points[ i].z;
    }
//    ROS_INFO("FINISHED POINT %d",i);
  }
  ROS_INFO("AFTER: ZMAX = %f, ZMIN = %f", zMax, zMin);
  ROS_INFO("ABOUT TO GET EIGENVECTORS");
  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*pcPtr, centroid);
  Eigen::Matrix3f covariance;
  pcl::computeCovarianceMatrixNormalized(*pcPtr, centroid, covariance);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigenSolver(covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix3f eigVecs = eigenSolver.eigenvectors();
  eigVecs.col(2) = eigVecs.col(0).cross(eigVecs.col(1));

  ROS_INFO("TRANSFORMING FRAME");
  Eigen::Matrix4f transform(Eigen::Matrix4f::Identity());
  transform.block<3,3>(0,0) = eigVecs.transpose();
  transform.block<3,1>(0,3) =  -1.f * (transform.block<3,3>(0,0) * centroid.head<3>());
  pcl::PointCloud<pcl::PointXYZRGB> cPoints;
  pcl::transformPointCloud(*pcPtr, cPoints, transform);

  pcl::PointXYZRGB minPt, maxPt;
  pcl::getMinMax3D(cPoints, minPt, maxPt);
  const Eigen::Vector3f meanDiag = 0.5f*(maxPt.getVector3fMap() + minPt.getVector3fMap());

  const Eigen::Quaternionf quat(eigVecs);
  const Eigen::Vector3f trans = eigVecs*meanDiag + centroid.head<3>();

  ros::Publisher box_pub = node.advertise<visualization_msgs::Marker>("visualization_marker_box", 1);
  ros::Publisher line_pub = node.advertise<visualization_msgs::Marker>("visualization_marker_line", 1);
  visualization_msgs::Marker marker;
  marker.header.frame_id = "base_link";
  marker.header.stamp = ros::Time::now();
  marker.ns = "basic_shapes";
  marker.id = 0;
  marker.type = visualization_msgs::Marker::CUBE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = trans(0);
  marker.pose.position.y = trans(1);
  marker.pose.position.z = pcSrv.response.table_tf.translation.z + (zMax-zMin)/2.0;//zMin;
  Eigen::Vector4f coeffs = quat.coeffs();
  marker.pose.orientation.x = coeffs(0);
  marker.pose.orientation.y = coeffs(1);
  marker.pose.orientation.z = coeffs(2);
  marker.pose.orientation.w = coeffs(3);
//  marker.scale.x = maxPt.x-minPt.x;
  marker.scale.x = zMax-zMin;//maxPt.z-minPt.z;
  marker.scale.y = maxPt.y-minPt.y;
  ROS_INFO("maxPt.x-minPt.x = %f",maxPt.x-minPt.x);
  ROS_INFO("maxPt.y-minPt.y = %f",maxPt.y-minPt.y);
  ROS_INFO("ZMAX-ZMIN=%f",zMax-zMin);
  marker.scale.z = maxPt.z-minPt.z;//zMax-zMin;
  ROS_INFO("marker.scale.z = %f",marker.scale.z);
  marker.color.r=1.0f;
  marker.color.g=0.0f;
  marker.color.b=0.0f;
  marker.color.a=0.75;

  marker.lifetime = ros::Duration();

//  tf::Quaternion yQuat(0.0, 1/std::sqrt(2),0.0,1/std::sqrt(2)); // For side grasp
	tf::Quaternion yQuat(0.0, 1.0,0.0,0.0); // For top grasp
  tf::Quaternion tfQuat(coeffs(0),coeffs(1),coeffs(2),coeffs(3));
//  tf::quaternionEigenToTF(quat, tfQuat);
 
  geometry_msgs::Quaternion finalQuat;
  tfQuat = tfQuat*yQuat;
  double roll, pitch, yaw;
  tf::Matrix3x3(tfQuat).getRPY(roll,pitch,yaw);
//  if(yaw >= M_PI/2.0 || yaw <= -(M_PI/2.0)) { // For side grasp
//	tf::Quaternion tempQuat(0.0, 1.0,0.0,0.0);
//	tfQuat = tfQuat*tempQuat;
//	ROS_INFO("REVERSED QUATERNION");
//  }
  tf::quaternionTFToMsg(tfQuat,finalQuat);


  visualization_msgs::Marker line;
  line.header.frame_id = "base_link";
  line.header.stamp = ros::Time::now();
  line.ns = "basic_shapes";
  line.id = 1;
  line.type = visualization_msgs::Marker::ARROW;
  line.action = visualization_msgs::Marker::ADD;
  line.pose.position.x = trans(0);
  line.pose.position.y = trans(1);
  line.pose.position.z = pcSrv.response.table_tf.translation.z + (zMax-zMin)/2.0;
//  line.pose.orientation.x = coeffs(0);
//  line.pose.orientation.y = coeffs(1);
//  line.pose.orientation.z = coeffs(2);
// line.pose.orientation.w = coeffs(3);
  line.pose.orientation = finalQuat;  // This is orientation for gripper
  line.scale.x = zMax-zMin;
  line.scale.y = 0.01;
  line.scale.z = 0.01;
  line.color.r=0.0;
  line.color.g=0.0;
  line.color.b=1.0;
  line.color.a=0.75;
  line.lifetime = ros::Duration();
  // Gripper position
  // Normalize first eigenvector, then scale it by (maxPt.z-minPt.z)/2 + offset from object in gripper's x direction
  Eigen::Vector3f dir = eigVecs.col(2);
  ROS_INFO("First eig vec: x = %f, y = %f, z = %f",eigVecs(0,0),eigVecs(1,0),eigVecs(2,0));
  ROS_INFO("Second eig vec: x = %f, y = %f, z = %f", eigVecs(0,1),eigVecs(1,1), eigVecs(2,1));
  ROS_INFO("Third eig vec: x = %f, y = %f, z = %f", eigVecs(0,2),eigVecs(1,2), eigVecs(2,2));
  dir.normalized();
  ROS_INFO("Normalized dir: x = %f, y = %f, z = %f",dir(0),dir(1),dir(2));

  ros::Publisher pose_pub = node.advertise<geometry_msgs::PoseStamped>("gripper_pose", 1);
  dir = dir *((maxPt.z-minPt.z)/2.0+0.22);
  ROS_INFO("Scaled dir: x = %f, y = %f, z = %f",dir(0),dir(1),dir(2));
  geometry_msgs::PoseStamped gripPose;
  gripPose.header.frame_id = "base_link";
  gripPose.header.stamp = ros::Time(0);
  ROS_INFO("trans: x = %f, y = %f, z = %f; dir: x = %f, y = %f, z = %f",trans(0),trans(1), 0.0, dir(0), dir(1), dir(2));
  if(dir(0) < 0) {
    dir(0) = dir(0)*-1;
    dir(1) = dir(1)*-1;
  }
//  gripPose.pose.position.x = trans(0)-dir(0); // For side grasp
//  gripPose.pose.position.y = trans(1)-dir(1);
//  gripPose.pose.position.z = pcSrv.response.table_tf.translation.z + (zMax-zMin)/2.0;

	gripPose.pose.position.x = trans(0);
	gripPose.pose.position.y = trans(1);
	gripPose.pose.position.z = zMax + 0.22;

  gripPose.pose.orientation = finalQuat;
  // Subtract result from centroid to get position of gripper


  // Publish the marker
  while (box_pub.getNumSubscribers() < 1) {
    if (!ros::ok()){
      return 0;
    }
    ROS_WARN_ONCE("Please create a subscriber to the marker");
    sleep(1);
  }
  while(ros::ok()) {
    box_pub.publish(marker);
    line_pub.publish(line);    
    pubOrigCloud.publish(pcSrv.response.original_cloud);
    pose_pub.publish(gripPose);
    ros::Duration(5.0).sleep();
  }


//  ROS_INFO("GOING TO BRING UP VIEWER");
//  pcl::visualization::PCLVisualizer viewer;
//  viewer.addPointCloud(pcPtr);
//  viewer.addCube(trans,quat,maxPt.x-minPt.x,maxPt.y-minPt.y,maxPt.z-minPt.z);
//  viewer.spin();
}
