#!/usr/bin/env python
# Visualize the sensor data by subsribing to the sensor topic
# Created by Patrick Lancaster (planc509@cs.washington.edu)
# Dervived from code provided by Liang-Ting Jiang (jianglt@uw.edu)

from Tkinter import Tk, Canvas, Frame, BOTH
import rospy
from pr2_pretouch_optical_dist.msg import OpticalDist

TOPIC_LEFT = 'optical/left'
TOPIC_RIGHT = 'optical/right'
SENSOR_COUNT = 6

class OpticalBeamsGUI(Frame):
  
    def __init__(self, parent):
        Frame.__init__(self, parent)   
        self.parent = parent        
        self.r = []
        self.text = []
        self.initUI()

    def beamCallbackLeft(self, msg):
        self.updateGUI(msg, 'l')

    def beamCallbackRight(self, msg):
        self.updateGUI(msg, 'r')
    
    def updateGUI(self, msg, side='r'):
        i = SENSOR_COUNT if side=='l' else 0

        self.canvas.itemconfigure(self.r[0+i], fill=('red' if msg.centerData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[0+i], text = str(msg.centerData[0]) + ',\n' + str(msg.centerData[1]))			

        self.canvas.itemconfigure(self.r[1+i], fill=('red' if msg.leftBackLateralData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[1+i], text = str(msg.leftBackLateralData[0]) + ',\n' + str(msg.leftBackLateralData[1]))	

        self.canvas.itemconfigure(self.r[2+i], fill=('red' if msg.leftFrontLateralData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[2+i], text = str(msg.leftFrontLateralData[0]) + ',\n' + str(msg.leftFrontLateralData[1]))	

        self.canvas.itemconfigure(self.r[3+i], fill=('red' if msg.frontTipData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[3+i], text = str(msg.frontTipData[0]) + ',\n' + str(msg.frontTipData[1]))	

        self.canvas.itemconfigure(self.r[4+i], fill=('red' if msg.rightBackLateralData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[4+i], text = str(msg.rightBackLateralData[0]) + ',\n' + str(msg.rightBackLateralData[1]))	

        self.canvas.itemconfigure(self.r[5+i], fill=('red' if msg.rightFrontLateralData[0] < 255 else 'blue'))
        self.canvas.itemconfigure(self.text[5+i], text = str(msg.rightFrontLateralData[0]) + ',\n' + str(msg.rightFrontLateralData[1]))		
            
    def initUI(self):
        self.parent.title("Optical Beams")        
        self.pack(fill=BOTH, expand=1)
        self.canvas = Canvas(self)

        # Right Title
        self.canvas.create_text(325, 50, text='r_gripper (Dist, ALS)', font=("Helvetica",30))
        
        self.r.append(self.canvas.create_rectangle(250, 275, 400, 425, fill="blue"))
        self.canvas.create_text(325, 275, text='CNT:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(325, 350, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(100, 375, 200, 525, fill="blue"))
        self.canvas.create_text(150, 375, text='LBL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(150, 450, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(100, 175, 200, 325, fill="blue"))
        self.canvas.create_text(150, 175, text='LFL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(150, 250, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(225, 105, 425, 205, fill="blue"))
        self.canvas.create_text(325, 105, text='FRT:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(325, 165, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(450, 375, 550, 525, fill="blue"))
        self.canvas.create_text(500, 375, text='RBL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(500, 450, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(450, 175, 550, 325, fill="blue"))
        self.canvas.create_text(500, 175, text='RFL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(500, 250, text='0, \n0', font=("Helvetica",18)))

        # Left Title
        self.canvas.create_text(900, 50, text='l_gripper (Dist, ALS)', font=("Helvetica",30))
        
        self.r.append(self.canvas.create_rectangle(825, 275, 975, 425, fill="blue"))
        self.canvas.create_text(900, 275, text='CNT:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(900, 350, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(675, 375, 775, 525, fill="blue"))
        self.canvas.create_text(725, 375, text='LBL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(725, 450, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(675, 175, 775, 325, fill="blue"))
        self.canvas.create_text(725, 175, text='LFL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(725, 250, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(800, 105, 1000, 205, fill="blue"))
        self.canvas.create_text(900, 105, text='FRT:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(900, 165, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(1025, 375, 1125, 525, fill="blue"))
        self.canvas.create_text(1075, 375, text='RBL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(1075, 450, text='0, \n0', font=("Helvetica",18)))

        self.r.append(self.canvas.create_rectangle(1025, 175, 1125, 325, fill="blue"))
        self.canvas.create_text(1075, 175, text='RFL:', font=("Helvetica",18), anchor='n')
        self.text.append(self.canvas.create_text(1075, 250, text='0, \n0', font=("Helvetica",18)))

        self.canvas.pack(fill=BOTH, expand=1)

    def start(self):
        rospy.loginfo('about to subscribe')
        self.sub1 = rospy.Subscriber(TOPIC_LEFT, OpticalDist, self.beamCallbackLeft)
        self.sub2 = rospy.Subscriber(TOPIC_RIGHT, OpticalDist, self.beamCallbackRight)

def main():
    rospy.init_node("optical_beams_gui", anonymous=True)
    rospy.loginfo('in main')
    root = Tk()
    ob = OpticalBeamsGUI(root)
    root.geometry("1225x600+300+300")
    ob.start()
    root.mainloop()  

if __name__ == '__main__':
    main()  
