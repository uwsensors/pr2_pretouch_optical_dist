#ifndef OPTICAL_GMM_H
#define OPTICAL_GMM_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <string>
#include <sensor_msgs/PointCloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;
typedef pcl::PointXYZRGB Point;

class OpticalGMM {

	private:Eigen::MatrixXd* xPtr;
		Eigen::MatrixXd* posteriorsPtr;
		Eigen::VectorXd* wPtr;
		Eigen::MatrixXd* meansPtr;
		Eigen::MatrixXd* sigPtr;
		int K;
		bool useX;
		bool useY;
		bool useZ;
		void split(const std::string& s, char c, std::vector<std::string>& v);
		void getLikelihood(double& likelihood, bool debug=false);
		void readData(std::string inFile, int K);
		void writeData();
		double GMMHelper();
		void initParams(); 
	public: OpticalGMM();
		~OpticalGMM(); 
		double GMMsolve(PointCloud::Ptr pcPtr, int K, bool useX=true, bool useY=true, bool useZ=true);
		void loadFromFile();
		std::vector<int> predict(std::vector<geometry_msgs::Point32> points);
};

#endif
