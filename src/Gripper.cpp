#include <pr2_pretouch_optical_dist/Gripper.h>


	
void Gripper::send(pr2_controllers_msgs::Pr2GripperCommandGoal& command, bool async) {
	gripperClient->sendGoal(command);
	if(!async) {
		gripperClient->waitForResult();
	}
//	gripperClient->waitForResult();
//	if(gripperClient->getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
//		ROS_INFO("Successfully moved gripper");
//	} else {
//		ROS_INFO("Failed to move gripper");
//	}
}
	
Gripper::Gripper(std::string side) {
	std::string sideBegin(1,side[0]);
	gripperClient = new GripperClient(sideBegin+"_gripper_controller/gripper_action", true);

	while(!gripperClient->waitForServer(ros::Duration(5.0))) {
		ROS_INFO("Waiting for the r_gripper_controller/gripper_action action server to come up");
	}
}

Gripper::~Gripper() {
	delete gripperClient;
}

bool Gripper::isDone() {
/*	if(gripperClient->getState() == actionlib::SimpleClientGoalState::ACTIVE) {
		ROS_ERROR("GOAL IS ACTIVE");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::PENDING) {
		ROS_ERROR("GOAL IS PENDING");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::RECALLED) {
		ROS_ERROR("GOAL IS RECALLED");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::REJECTED) {
		ROS_ERROR("GOAL IS REJECTED");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::PREEMPTED) {
		ROS_ERROR("GOAL IS PREEMPTED");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::ABORTED) {
		ROS_ERROR("GOAL IS ABORTED");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
		ROS_ERROR("GOAL IS SUCCEEDED");
	} else if(gripperClient->getState() == actionlib::SimpleClientGoalState::LOST) {
		ROS_ERROR("GOAL IS LOST");
	} else {
		ROS_ERROR("DIDN'T FIND A MATCH");
	} */

	return gripperClient->getState() != actionlib::SimpleClientGoalState::ACTIVE && gripperClient->getState() != actionlib::SimpleClientGoalState::PENDING;
}

void Gripper::open(double effort,bool async,double position) {
	// min effort is ~ 8.1
	pr2_controllers_msgs::Pr2GripperCommandGoal open;
	open.command.position = std::min(position,0.085);
	open.command.max_effort = effort;
	ROS_INFO("Sending open goal");
	send(open,async);
}

void Gripper::stop() {
	gripperClient->cancelGoal();
}

void Gripper::close(double effort,bool async,double position) {
	pr2_controllers_msgs::Pr2GripperCommandGoal close;
	close.command.position = std::max(position, 0.0);
	close.command.max_effort = effort;
	ROS_INFO("Sending close goal");
	send(close, async);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "simple_gripper");

  Gripper gripper;

  gripper.open();
  gripper.close();

  return 0;
}
