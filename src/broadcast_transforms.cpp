#include <stdlib.h>
#include <string>
#include <vector>

#include "tf/transform_broadcaster.h"

#define N_TRANSFORMS 6
std::vector<tf::StampedTransform*> transforms(N_TRANSFORMS);

int main(int argc, char** argv) {
	ros::init(argc, argv, "optical sensor transform publisher", ros::init_options::AnonymousName);
	ros::NodeHandle node;


	tf::TransformBroadcaster broadcaster;
	std::string side = argv[1];
	std::string sideBegin(1,side[0]);

	for(int i = 0; i < N_TRANSFORMS; i++) {
		transforms[i] = new tf::StampedTransform();
	}

	std::string frameId = sideBegin+"_gripper_l_finger_tip_link";

	tf::Quaternion q0;
	q0.setRPY(0.0,0.0,0.0);
	tf::Transform ctr(q0, tf::Vector3(0.0,0.0,0.0));
	transforms[0]->setData(ctr);
	transforms[0]->frame_id_ = frameId;
	transforms[0]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_ctr_link";

	tf::Quaternion q1;
	q1.setRPY(0.0,0.0,0.0);
	tf::Transform lbl(q1, tf::Vector3(0.0,0.0,0.0));
	transforms[1]->setData(lbl);
	transforms[1]->frame_id_ = frameId;
	transforms[1]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_lbl_link";

	tf::Quaternion q2;
	q2.setRPY(0.0,0.0,0.0);
	tf::Transform lfl(q2, tf::Vector3(0.0,0.0,0.0));
	transforms[2]->setData(lfl);
	transforms[2]->frame_id_ = frameId;
	transforms[2]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_lfl_link";

	tf::Quaternion q3;
//	q3.setRPY(-1.5708, 3.1415, 3.1415);
	q3.setRPY(0,0,0.0);
	tf::Transform frt(q3, tf::Vector3(0.030175, -0.008, 0.00118802851));
	transforms[3]->setData(frt);
	transforms[3]->frame_id_ = frameId;
	transforms[3]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_frt_link";

	tf::Quaternion q4;
	q4.setRPY(0.0,0.0,0.0);
	tf::Transform rbl(q4, tf::Vector3(0.0,0.0,0.0));
	transforms[4]->setData(rbl);
	transforms[4]->frame_id_ = frameId;
	transforms[4]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_rbl_link";

	tf::Quaternion q5;
	q5.setRPY(0.0,0.0,0.0);
	tf::Transform rfl(q5, tf::Vector3(0.0,0.0,0.0));
	transforms[5]->setData(rfl);
	transforms[5]->frame_id_ = frameId;
	transforms[5]->child_frame_id_ = sideBegin+"_gripper_l_finger_optical_rfl_link";

	ros::Duration sleeper(atof(argv[2])/1000.0);

	while(node.ok()) {

		for(int i = 0; i < N_TRANSFORMS; i++) {
			transforms[i]->stamp_ = ros::Time::now()+sleeper;
			broadcaster.sendTransform(*(transforms[i]));
			
		}
		sleeper.sleep();
	}
}
