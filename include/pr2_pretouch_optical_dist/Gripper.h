#ifndef GRIPPER_H
#define GRIPPER_H

#include <ros/ros.h>
#include <pr2_controllers_msgs/Pr2GripperCommandAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/simple_client_goal_state.h>



typedef actionlib::SimpleActionClient<pr2_controllers_msgs::Pr2GripperCommandAction> GripperClient;

class Gripper{

	private: GripperClient* gripperClient;
	
		void send(pr2_controllers_msgs::Pr2GripperCommandGoal& command, bool async);
	public:
		Gripper(std::string side="r");

		~Gripper();

		void open(double effort=-1.0,bool async=true,double position=0.085);
		void stop();
		void close(double effort=-1.0,bool async=true,double position=0.0);
		bool isDone();
};


#endif
