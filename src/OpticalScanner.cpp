#include <pr2_pretouch_optical_dist/OpticalScanner.h>

static const double EDGE_THRESHOLD = 30.0;
static const double EDGE_EFFORT = -1.0;
OpticalScanner::OpticalScanner(ros::NodeHandle* node, std::string& groupName, std::string& side, std::vector<int>* thresholds) {
	
	// Create optical listener that will record scanning data
	optListenPtr = new OpticalListener(node, side);
	// Left or right side
	this->side = side;

	// Turn on the front channel if no thresholds were passed
	if(thresholds == NULL) {
		ROS_INFO("Nothing passed for thresholds argument. Turning on front channel only");
		optListenPtr->frontTipActive(true,255);
		
	} else {
		optListenPtr->setThresholds(thresholds);
	}

	
	tfListenerPtr = new tf::TransformListener();
	groupPtr = new moveit::planning_interface::MoveGroup(groupName);
	groupPtr->setGoalOrientationTolerance(0.0314);
	// Used to spin the gripper
	std::string sideBegin(1,side[0]);
  trajClient = new TrajClient(sideBegin+"_arm_controller/joint_trajectory_action", false);

	int gripJointIdx;
	if(side.compare("left") == 0) {
		gripJointIdx = 38;
	} else {
		gripJointIdx = 24;
	}
	curGripWidth = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripJointIdx];
}

OpticalScanner::~OpticalScanner() {
	delete optListenPtr;
	delete groupPtr;
	delete tfListenerPtr;
	delete trajClient;
}

void OpticalScanner::subCallback(const sensor_msgs::JointState msg) {

	curGripWidth = msg.position[gripJointIdx];

}

// Gets the average value of the front sensor over n samples and adds the passed offset to get the returned threshold
double OpticalScanner::getThreshold(int n, double offset) {
	// Get baseline
	double xAvg = 0.0;
	for(int i = 0; i < n; i ++) {
		pr2_pretouch_optical_dist::OpticalDistConstPtr msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		xAvg += msg->frontTipData[0];
	}
	xAvg = xAvg / n;
	return std::min(xAvg + offset,254.0);
}

// Helper method for when an object is initially detected
geometry_msgs::PoseStamped OpticalScanner::initDetectScan(double* threshold, int n, double offset) {
	// The index in joint_states topic corresponding to either l_gripper_joint or r_gripper_joint
	int gripWidthIdx;
	std::string sideBegin(1,side[0]);
	if(side.compare("left") == 0) {
		gripWidthIdx = 38;
	} else {
		gripWidthIdx = 24;
	}
	// Get the position when the gripper is open and calculate the threshold
	ROS_ERROR("ABOUT TO WAIT FOR MESSAGE IN INITDETECTSCAN");
	double openPos = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];
	*threshold = OpticalScanner::getThreshold(n, offset);

	// Slowly close the gripper
	Gripper gripper(side);
	ROS_ERROR("ABOUT TO CLOSE GRIPPER");
	gripper.close(11.0);

	// Wait until an edge is seen
	ros::Time startTime = ros::Time::now();
	ros::Duration timeout(15.0);
	bool foundEdge = false;
	double edgePos;
	while(ros::Time::now() - startTime < timeout) {
		edgePos = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];
  		pr2_pretouch_optical_dist::OpticalDistConstPtr msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		if(msg->frontTipData[0] >= *threshold) {
			edgePos += ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];
			gripper.stop();
			foundEdge = true;
			edgePos = edgePos / 2.0;
			break;
		}
		
	}
	
	// Spin the gripper
	std::vector<double> curJointVals = groupPtr->getCurrentJointValues();	
	std::vector<std::string> jointNames = groupPtr->getJoints();
	jointNames.erase(jointNames.begin()+6);
	jointNames.erase(jointNames.begin()+3);
	pr2_controllers_msgs::JointTrajectoryGoal spinGoal;
	for(int i = 0; i < jointNames.size(); i++) {
		spinGoal.trajectory.joint_names.push_back(jointNames[i]);
	}
	curJointVals = groupPtr->getCurrentJointValues();	
	spinGoal.trajectory.points.resize(1);
	spinGoal.trajectory.points[0].positions.resize(7);
	spinGoal.trajectory.points[0].velocities.resize(7);
	for(int i = 0; i < curJointVals.size(); i++) {
		spinGoal.trajectory.points[0].positions[i] = curJointVals[i];
		spinGoal.trajectory.points[0].velocities[i] = 0.0005;
	}
	spinGoal.trajectory.points[0].time_from_start = ros::Duration(2.0);
	spinGoal.trajectory.points[0].positions[6] -= 3.14159;	
	spinGoal.trajectory.header.stamp = ros::Time::now();
    	while(!trajClient->waitForServer(ros::Duration(5.0))){
   		ROS_ERROR("Waiting for the joint_trajectory_action server");
    	}
	trajClient->sendGoal(spinGoal);
	gripper.open();
	trajClient->waitForResult();
	
	geometry_msgs::PoseStamped result;

	// If edge found, then send pose that aligns fingertip with edge
	if(foundEdge) {
		result.header.stamp = ros::Time(0);
		result.header.frame_id = sideBegin+"_wrist_roll_link";
		result.pose.position.x = 0.0;
		result.pose.position.y = -(openPos - (openPos-edgePos)/2.0 + 0.01);
		result.pose.position.z = 0.0;
		result.pose.orientation.x = 0.0;
		result.pose.orientation.y = 0.0;
		result.pose.orientation.z = 0.0;
		result.pose.orientation.w = 1.0;
		return result;
	} else { // If edge not found, object is either not graspable, or reduces to initNoDetectScan
		pr2_pretouch_optical_dist::OpticalDistConstPtr detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		if(detectMsg->frontTipData[0] < *threshold) {
			// Object not graspable, return bad pose (sum of square quaternion components != 1)
			ROS_ERROR("NOT GRASPABLE");
			return result;
		} else {
			return OpticalScanner::initNoDetectScan(threshold, n, offset);
		}
	}
	

}

// Helper method for when an object is not initially detected
geometry_msgs::PoseStamped OpticalScanner::initNoDetectScan(double* threshold, int n, double offset) {
	// The index in joint_states topic corresponding to either l_gripper_joint or r_gripper_joint
	int gripWidthIdx;
	std::string sideBegin(1,side[0]);
	if(side.compare("left") == 0) {
		gripWidthIdx = 38;
	} else {
		gripWidthIdx = 24;
	}
	ROS_ERROR("ABOUT TO WAIT FOR MESSAGE IN INITNODETECTSCAN");
	double openPos = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];

	// Close the gripper slowly
	ROS_ERROR("ABOUT TO CLOSE GRIPPER");
	Gripper gripper(side);
	gripper.close(11.0);

	ros::Time startTime = ros::Time::now();
	ros::Duration timeout(15.0);
	bool foundEdge = false;
	double edgePos;

	// Use default threshold if not given one
	if(*threshold < 0.0) {
		*threshold = 100.0;
	}

	// Wait until an edge is found
	while(ros::Time::now() - startTime < timeout) {
		edgePos = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];
  		pr2_pretouch_optical_dist::OpticalDistConstPtr msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		if(msg->frontTipData[0] < *threshold) {
			edgePos += ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripWidthIdx];
			gripper.stop();
			foundEdge = true;
			edgePos = edgePos / 2.0;
			break;
		}
		
	}
	
	// Open the gripper
	gripper.open(-1.0,true);
	
	geometry_msgs::PoseStamped result;
	result.header.stamp = ros::Time(0);
	result.header.frame_id = sideBegin+"_wrist_roll_link";
	result.pose.position.x = 0.0;
	result.pose.position.y = 0.0;
	result.pose.position.z = 0.0;
	result.pose.orientation.x = 0.0;
	result.pose.orientation.y = 0.0;
	result.pose.orientation.z = 0.0;
	result.pose.orientation.w = 1.0;
	
	if(!foundEdge) {
		// Move wrist to be between fingertips
		result.pose.position.y = -0.0425;
		return result;
	} 

	// Now that edge is found, get threshold
	*threshold = OpticalScanner::getThreshold(n, offset);

	// Place fingertip on edge
	result.pose.position.y = -((openPos-edgePos)/2.0) ;

	return result;	
} 

// Aligns the fingertip with an edge and then performs a scan
std::vector<geometry_msgs::Point> OpticalScanner::horizontalScan(double velScale) {
	// Start AsyncSpinner for MoveIt
	ROS_ERROR("IN HORIZONTAL SCAN");
	ros::AsyncSpinner spinner(1);
	spinner.start();

	// The index in joint_states topic corresponding to either l_gripper_joint or r_gripper_joint
	int gripWidthIdx;
	std::string sideBegin(1,side[0]);
	if(side.compare("left") == 0) {
		gripWidthIdx = 38;
	} else {
		gripWidthIdx = 24;
	}
	Gripper gripper(side);

	// Determine whether there is an object in front of the fingertip
	int nSamples = 15;
	double offset = 25.0;
	geometry_msgs::PoseStamped startScan;
	pr2_pretouch_optical_dist::OpticalDistConstPtr detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
	double threshold = detectMsg->frontTipData[0];
	bool detect = threshold < 100.0;

	std::vector<double> curJointVals;	
	std::vector<std::string> jointNames;

	// Get pose that will align fingertip with edge
	if(detect) {
		ROS_ERROR("ABOUT TO GO INTO INITDETECTSCAN");
		startScan = OpticalScanner::initDetectScan(&threshold, nSamples, offset);
	} else {
		ROS_ERROR("ABOUT TO GO INTO INITNODETECTSCAN");
		startScan = OpticalScanner::initNoDetectScan(&threshold, nSamples, offset);
	}

	// Create way points so fingetip goes to edge and gripper rotates
	geometry_msgs::PoseStamped midScan;
	midScan.header.stamp = ros::Time(0);
	midScan.header.frame_id = sideBegin+"_wrist_roll_link";
	midScan.pose.position.x = 0.0;
	midScan.pose.position.y = startScan.pose.position.y;
	midScan.pose.position.z = 0.0;
	tf::Quaternion q;
	double roll = -1.5708;
	if(detect) {
		roll *= -1.0;
	}
	q.setRPY(roll, 0.0, 0.0);
	geometry_msgs::Quaternion quat;
	tf::quaternionTFToMsg(q, quat);
	midScan.pose.orientation = quat;

	
	geometry_msgs::PoseStamped endScan;
	endScan.header.stamp = ros::Time(0);
	endScan.header.frame_id = sideBegin+"_wrist_roll_link";
	endScan.pose.position.x = 0.0;
	endScan.pose.position.y = startScan.pose.position.y;
	endScan.pose.position.z = 0.0;
	endScan.pose.orientation.x = -1.0;
	endScan.pose.orientation.y = 0.0;	
	endScan.pose.orientation.z = 0.0;
	endScan.pose.orientation.w = 0.0;

	std::vector<geometry_msgs::PoseStamped> goals(0);
	// Make sure a valid scan was passed back
	double quatCheck = std::pow(startScan.pose.orientation.x,2) + std::pow(startScan.pose.orientation.y,2) + std::pow(startScan.pose.orientation.z,2) + std::pow(startScan.pose.orientation.w,2); 
	bool scanReady = quatCheck <= 1.1 && quatCheck >= 0.9;
	if(scanReady) {
//		goals.push_back(startScan);
		goals.push_back(midScan);
		goals.push_back(endScan);
		optListenPtr->frontTipActive(true, threshold);
		optListenPtr->start();	
		OpticalScanner::waypointsMove(goals, false, velScale);
		optListenPtr->stop();	

	}

	// Spin the gripper back to its original orientation if necessary
	if(!detect || !scanReady) {
		curJointVals = groupPtr->getCurrentJointValues();	
		jointNames = groupPtr->getJoints();

		jointNames.erase(jointNames.begin()+6);
		jointNames.erase(jointNames.begin()+3);
		pr2_controllers_msgs::JointTrajectoryGoal resetGoal;
		for(int i = 0; i < jointNames.size(); i++) {
			resetGoal.trajectory.joint_names.push_back(jointNames[i]);
		}
		curJointVals = groupPtr->getCurrentJointValues();	
		resetGoal.trajectory.points.resize(1);
		resetGoal.trajectory.points[0].positions.resize(7);
		resetGoal.trajectory.points[0].velocities.resize(7);
		for(int i = 0; i < curJointVals.size(); i++) {
			resetGoal.trajectory.points[0].positions[i] = curJointVals[i];
			resetGoal.trajectory.points[0].velocities[i] = 0.0005;
		}
		resetGoal.trajectory.points[0].time_from_start = ros::Duration(2.0);
		resetGoal.trajectory.points[0].positions[6] -= 3.14159;	
		resetGoal.trajectory.header.stamp = ros::Time::now();

    		while(!trajClient->waitForServer(ros::Duration(5.0))){
     		 ROS_ERROR("Waiting for the joint_trajectory_action server");
    		}
		trajClient->sendGoal(resetGoal);
		trajClient->waitForResult();
	}


	std::vector<geometry_msgs::Point> results;
	if(!scanReady) { // Return empty results if scan not successful
		return results;
	}
	// Order the points by y-coordinate
	results = getOrderedScanPoints(optListenPtr->getPointCloud(sideBegin+"_wrist_roll_link"));

	// Offset
	for(int i = 0; i < results.size(); i++) {
		if(detect) {
//			results[i].y -= 0.005;
		} else { 
//			results[i].y += 0.005;
		}		
		
	}
	return results;

}


geometry_msgs::Point OpticalScanner::edgeScan() {
	ROS_ERROR("IN OPTICAL::SCANNER EDGE SCAN");
	ros::AsyncSpinner spinner(1);
	spinner.start();

	std::string sideBegin(1,side[0]);
	geometry_msgs::PointStamped result;
	result.header.stamp = ros::Time(0);
	result.header.frame_id = sideBegin+"_wrist_roll_link";
	Gripper gripper(side);
	gripper.open(-1.0, false, 0.085);
	int gripJointIdx;
	if(side.compare("left") == 0) {
		gripJointIdx = 38;
	} else {
		gripJointIdx = 24;
	}
	double gripOpenWidth = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states")->position[gripJointIdx];
	
	ROS_ERROR("GETTING INITIAL READING");
	pr2_pretouch_optical_dist::OpticalDistConstPtr detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
	int initRead = detectMsg->frontTipData[0];
	if(initRead <= 50) {
		OpticalScanner::spinGripper(M_PI,0.25,false);
		detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		initRead = detectMsg->frontTipData[0];
	}
	
	
	int curRead = -1;

	double searchAngle = M_PI;
	geometry_msgs::PointStamped firstEdge;
	firstEdge.header.stamp = ros::Time(0);
	firstEdge.header.frame_id = "base_link";
	ROS_ERROR("LOOKING FOR FIRST EDGE");
	while(curRead < 0) {
		ROS_ERROR("ABOUT TO CALL FIND EDGE");
		firstEdge.point = OpticalScanner::findEdge(&gripper, initRead, &curRead, EDGE_THRESHOLD, EDGE_EFFORT);
		if(curRead >= 0) {
			ROS_ERROR("FOUND FIRST EDGE");
			ROS_ERROR("x = %f, y = %f, z = %f", firstEdge.point.x,firstEdge.point.y, firstEdge.point.z);
			break;
		} 
		gripper.open(-1.0, true,0.085);
		OpticalScanner::spinGripper(searchAngle, 0.25,false);
		searchAngle = searchAngle / 2;
		detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		initRead = detectMsg->frontTipData[0];
	}
	
	if(initRead > curRead) {
		ROS_ERROR("GOING TO SPIN GRIPPER");
		OpticalScanner::spinGripper(M_PI,0.25,false);
	}
	int objectRead = std::min(initRead, curRead);
	int tableRead = std::max(initRead, curRead);
	
	detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
	if(std::abs(detectMsg->frontTipData[0]-objectRead) < std::abs(detectMsg->frontTipData[0]-tableRead) || initRead <= curRead) {
		ROS_ERROR("CREATING firstTransEdge");		
		geometry_msgs::PointStamped firstTransEdge;
		geometry_msgs::PoseStamped edgePose;
		edgePose.header.stamp = ros::Time(0);
		edgePose.header.frame_id = sideBegin+"_wrist_roll_link";		
		gripper.open(-1.0, true, 0.085);
		ROS_ERROR("ABOUT TO PRINT FIRST EDGE INFO");
		ROS_ERROR("firstEdge in base_link: x = %f, y = %f, z = %f",firstEdge.point.x, firstEdge.point.y, firstEdge.point.z);
		ROS_ERROR("PINTED FIRST EDGE INFO");		
		tfListenerPtr->waitForTransform( "base_link", sideBegin+"_wrist_roll_link", ros::Time::now(), ros::Duration(1.0));
		tfListenerPtr->transformPoint(sideBegin+"_wrist_roll_link", firstEdge, firstTransEdge);	

		edgePose.pose.position.x = 0.0;
		edgePose.pose.position.y = gripOpenWidth/2 - std::abs(firstTransEdge.point.y);
		ROS_ERROR("GOING TO ADJUST POSE BY %f",edgePose.pose.position.y);
		edgePose.pose.position.z = 0.0;
		edgePose.pose.orientation.x = 0.0;
		edgePose.pose.orientation.y = 0.0;
		edgePose.pose.orientation.z = 0.0;
		edgePose.pose.orientation.w = 1.0;
		std::vector<geometry_msgs::PoseStamped> goals;
		goals.push_back(edgePose);
		ROS_ERROR("MOVING FINGERTIP TO EDGE");
		OpticalScanner::waypointsMove(goals, false, 1.0);
		detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		if(std::abs(detectMsg->frontTipData[0]-objectRead) < std::abs(detectMsg->frontTipData[0]-tableRead)) {
			ROS_ERROR("OBJECT NOT GRASPABLE");
			return result.point;
		}
		tableRead = detectMsg->frontTipData[0];
	} 

	ROS_ERROR("LOOKING FOR SECOND EDGE");
	geometry_msgs::PointStamped secondEdge;
	secondEdge.point = OpticalScanner::findEdge(&gripper,  tableRead, &objectRead, EDGE_THRESHOLD, EDGE_EFFORT);
	geometry_msgs::PointStamped tmpResult;
	tmpResult.header.stamp = ros::Time(0);
	tmpResult.header.frame_id = "base_link";
	tmpResult.point.x = (firstEdge.point.x + secondEdge.point.x) / 2.0;
	tmpResult.point.y = (firstEdge.point.y + secondEdge.point.y) / 2.0;
	tmpResult.point.z = std::max(firstEdge.point.z, secondEdge.point.z);
	tfListenerPtr->waitForTransform( "base_link", sideBegin+"_wrist_roll_link", ros::Time::now(), ros::Duration(1.0));
	tfListenerPtr->transformPoint(sideBegin+"_wrist_roll_link", tmpResult, result);
	gripper.open(-1.0, false, 0.085);
	spinner.stop();
	return result.point;
}

geometry_msgs::Point OpticalScanner::findEdge(Gripper* gripper, int initRead, int* finalRead, double threshold, double effort) {

	ROS_ERROR("IN FIND EDGE");
	//ros::Subscriber sub = node.subscribe("joint_states", 1000, &OpticalScanner::subCallback, this);// Get grip width
	ros::Time startTime = ros::Time::now();
	ros::Duration timeout(15.0);
	bool foundEdge = false;
	gripper->close(effort,true,0.0);
	ROS_ERROR("GOING TO CLOSE");
	pr2_pretouch_optical_dist::OpticalDistConstPtr msg;
	while(ros::Time::now() - startTime < timeout) {
  	msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		ROS_ERROR("FRONT TIP MEAS: %d", msg->frontTipData[0]);
		if((msg->frontTipData[0] >= initRead+threshold || msg->frontTipData[0] <= initRead-threshold) && msg->frontTipData[0] < 90) {
			ROS_ERROR("FOUND AN EDGE");
			gripper->stop();
			foundEdge = true;
			break;
		}
	}
	if(foundEdge) {
		*finalRead = msg->frontTipData[0];
	} else {
		*finalRead = -1;
	}
	ROS_ERROR("GOING TO RETURN THE POSITION: x = %f, y = %f, z= %f", msg->frontTipPos.x, msg->frontTipPos.y, msg->frontTipPos.z);
	return msg->frontTipPos;
}

void OpticalScanner::spinGripper(double angle, double duration, bool async) {

	static pr2_controllers_msgs::JointTrajectoryGoal spinGoal;
	static std::vector<double> curJointVals = groupPtr->getCurrentJointValues();	
	static std::vector<std::string> jointNames = groupPtr->getJoints();
	if(jointNames.size() > 7) {
		jointNames.erase(jointNames.begin()+6);
		jointNames.erase(jointNames.begin()+3);
	
		for(int i = 0; i < jointNames.size(); i++) {
			spinGoal.trajectory.joint_names.push_back(jointNames[i]);
		}
		spinGoal.trajectory.points.resize(1);
		spinGoal.trajectory.points[0].positions.resize(7);
		spinGoal.trajectory.points[0].velocities.resize(7);
	}

	curJointVals = groupPtr->getCurrentJointValues();	

	for(int i = 0; i < curJointVals.size(); i++) {
		spinGoal.trajectory.points[0].positions[i] = curJointVals[i];
		spinGoal.trajectory.points[0].velocities[i] = 0.0;
	}
	spinGoal.trajectory.points[0].time_from_start = ros::Duration(duration);
	spinGoal.trajectory.points[0].positions[6] += angle;	
	spinGoal.trajectory.header.stamp = ros::Time::now();
  while(!trajClient->waitForServer(ros::Duration(5.0))){
   		ROS_ERROR("Waiting for the joint_trajectory_action server");
  }
	trajClient->sendGoal(spinGoal);
	if(!async) {
		trajClient->waitForResult();
	}
}

// Moves the gripper towards the goal position until something is detected between the fingertips
bool OpticalScanner::graspDetect(geometry_msgs::PoseStamped goal, double threshold) {
	/// Start AsyncSpinner for MoveIt
	ros::AsyncSpinner spinner(1);
	spinner.start();
	if(threshold < 0) {
/*		double xAvg = 0.0;
		double xSum = 0.0;
		double xSqSum = 0.0;
		double xStd = 0.0;
		int n = 15;
		for(int i = 0; i < n; i ++) {
			pr2_pretouch_optical_dist::OpticalDistConstPtr msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
			float xI = msg->centerData[0];
			xSum += xI;
			xSqSum += xI*xI;
		}
		xAvg = xSum / n;
		xStd = sqrt(xSqSum/n - xSum*xSum/(n*n));
		threshold = xAvg - 3*xStd; */
		threshold = 70;
	}
	// Move horizontally before going in for grasp if possible
	std::vector<geometry_msgs::PoseStamped> goals;	
	geometry_msgs::PoseStamped midPose;
	midPose.header.stamp = ros::Time(0);
	midPose.header.frame_id = goal.header.frame_id;
	midPose.pose.position.x = 0.0;
	midPose.pose.position.y = goal.pose.position.y;
	midPose.pose.position.z = 0.0;
	midPose.pose.orientation.x = 0.0;
	midPose.pose.orientation.y = 0.0;
	midPose.pose.orientation.z = 0.0;
	midPose.pose.orientation.w = 1.0;
	goals.push_back(midPose);
	goals.push_back(goal);
	OpticalScanner::waypointsMove(goals, true, 0.1);

	// Execute until something seen between fingertips
	ros::Time startTime = ros::Time::now();
	ros::Duration timeout(15.0);
	bool detect = false;
	while(ros::Time::now() - startTime < timeout) {
  		pr2_pretouch_optical_dist::OpticalDistConstPtr msg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
		if(msg->centerData[0] < threshold) {
			groupPtr->stop();
			detect = true;
			break;
		}
		
	}
	return detect;
}

// Expects points in base_link
geometry_msgs::Quaternion OpticalScanner::getAlignOrientation(std::vector<double> x, std::vector<double> y) {
	double beta;
	double xySum = 0.0;
	double xSum = 0.0;
	double ySum = 0.0;
	double ySqSum = 0.0;
	int n = std::min(x.size(), y.size());	
	for(int i = 0; i < n; i++) {
		xySum += x[i]*y[i];
		xSum += x[i];
		ySum += y[i];
		ySqSum += y[i]*y[i];
	}
	beta = (xySum - xSum*ySum/n)/(ySqSum-ySum*ySum/n);
	beta = -1.0/beta;

	double y0 = 0.0;
	double y1 = 1.0;
	double x0 = 0.0;
	double x1 = y1*beta;

	double yaw = std::atan((y1-y0)/(x1-x0));
	ROS_ERROR("YAW = %f, BETA = %f", yaw,beta);
	tf::Quaternion q;
	q.setRPY(0.0, 0.0, yaw);
	geometry_msgs::Quaternion quat;
	tf::quaternionTFToMsg(q, quat);
	return quat;
	
}

// Moves to the goals passed in by waypoints
geometry_msgs::PoseStamped OpticalScanner::waypointsMove(std::vector<geometry_msgs::PoseStamped> goals, bool async, double velScale, bool collisionCheck) {
	std::vector<geometry_msgs::Pose> wayPoints(0);
//	geometry_msgs::PoseStamped temp;
//	if(goals.size() == 1) {
//		
//		temp.header.stamp = ros::Time(0);
//		temp.header.frame_id = goals[0].header.frame_id;
//		temp.pose.position.x = goals[0].pose.position.x;
//		temp.pose.position.y = 0.0;
//		temp.pose.position.z = goals[0].pose.position.z;
//		temp.pose.orientation.x = goals[0].pose.orientation.x;
//		temp.pose.orientation.y = goals[0].pose.orientation.y;
//		temp.pose.orientation.z = goals[0].pose.orientation.z;
//		temp.pose.orientation.w = goals[0].pose.orientation.w;
//		goals.insert(goals.begin(),temp);
//	}

	// Tranform goals to the planning frame
	tfListenerPtr->waitForTransform( goals[0].header.frame_id, groupPtr->getPlanningFrame(), ros::Time::now(), ros::Duration(1.0));
	for(int i = 0; i < goals.size(); i++) {
		goals[i].header.stamp = ros::Time(0);
		geometry_msgs::PoseStamped tmpPose;
		try {
			tfListenerPtr->transformPose(groupPtr->getPlanningFrame(),goals[i], tmpPose);

		} catch(tf::TransformException ex) {
			ROS_ERROR("%s", ex.what());
		}

		wayPoints.push_back(tmpPose.pose);
		
	}

	moveit_msgs::RobotTrajectory* trajPtr = new moveit_msgs::RobotTrajectory();
	
	// Try to compute a path
	double success = groupPtr->computeCartesianPath(wayPoints, 0.01, 0, *trajPtr, collisionCheck);
	groupPtr->clearPoseTargets();

	// Try removing some waypoints if plan cannot be found
	while(success < 0.75 && wayPoints.size() > 1) {
		wayPoints.erase(wayPoints.begin());
		success = groupPtr->computeCartesianPath(wayPoints, 0.01, 0, *trajPtr, collisionCheck);
	}

	// Compute velocities for found path
	if(velScale < 1.0) {
 		trajectory_processing::IterativeParabolicTimeParameterization tsHelp;
		robot_trajectory::RobotTrajectory armParams(groupPtr->getCurrentState()->getRobotModel(),side+"_arm");
		armParams.setRobotTrajectoryMsg(*(groupPtr->getCurrentState()), *trajPtr);
		bool tsComp = tsHelp.computeTimeStamps(armParams);
		tsHelp.computeTimeStamps(armParams);
		armParams.getRobotTrajectoryMsg(*trajPtr);
		OpticalScanner::scaleTrajectorySpeed(trajPtr, velScale);
	}

	moveit::planning_interface::MoveItErrorCode code = moveit_msgs::MoveItErrorCodes::FAILURE;
	moveit::planning_interface::MoveGroup::Plan plan;
	// Try to execute until success
	while(code != moveit_msgs::MoveItErrorCodes::SUCCESS) {
//		OpticalScanner::RemoveSpin(trajPtr);
		plan.trajectory_ = *trajPtr;
		if(async) {
			code = groupPtr->asyncExecute(plan);
		} else {
			code = groupPtr->execute(plan);
		}
	}


	return groupPtr->getCurrentPose();
}

geometry_msgs::PoseStamped OpticalScanner::getCurrentPose() {
	return groupPtr->getCurrentPose();
}

std::string OpticalScanner::getPoseReferenceFrame() {
	return groupPtr->getPoseReferenceFrame();
}

std::string OpticalScanner::getEndEffectorLink() {
	return groupPtr->getEndEffectorLink();
}

std::string OpticalScanner::getPlanningFrame() {
	return groupPtr->getPlanningFrame();
}

// Given a point cloud, return a list of points (ordered by y coordinate) that belong to the most scanned object
std::vector<geometry_msgs::Point> OpticalScanner::getOrderedScanPoints(PointCloud::Ptr pcPtr) {

	std::vector<std::vector<Point*>*>* medPoints = new std::vector<std::vector<Point*>*>();
	std::vector<Point*>* medSetPtr;
	for(int i = 0; i < pcPtr->points.size(); i++) {
		int idx = -1;
		// Find idx
		for(int j = medPoints->size()-1; j >= 0; j--) {
			medSetPtr = (*medPoints)[j];
			if(medSetPtr->size() > 0 && *reinterpret_cast<uint32_t*>(&((*medSetPtr)[0]->rgb)) == *reinterpret_cast<uint32_t*>(&(pcPtr->points[i].rgb))) {
				idx = j;
				break;
			}
		}
		// Idx not found, create new one
		if(idx == -1) {
			medPoints->push_back(new std::vector<Point*>());
			idx = medPoints->size() - 1;
		}
		
		Point * point = new Point();
		point->x = pcPtr->points[i].x;
		point->y = pcPtr->points[i].y;
		point->z = pcPtr->points[i].z;
		point->rgb = pcPtr->points[i].rgb;
		
		medSetPtr = (*medPoints)[idx];
		bool insert = true;
		std::vector<Point*>::iterator it = medSetPtr->end();
		for(int j = medSetPtr->size()-1; j >= 0; j--) {
			if(std::abs(pcPtr->points[i].y - (*medSetPtr)[j]->y) < 0.00001) {
				insert = false;
				break;
			}
			if(pcPtr->points[i].y > (*medSetPtr)[j]->y) {
				
				medSetPtr->insert(it-(medSetPtr->size()-1 - j),point);
				insert = false;
				break;
			}
		}
		// Either there are no points in medSetPtr or point is the smallest value
		if(insert) {
			it = medSetPtr->begin();
			medSetPtr->insert(it,point);
		}
		
	}
	
	int maxIdx = 0;
	int maxSize = 0;
	for(int i = 0; i < medPoints->size(); i++) {
		medSetPtr = (*medPoints)[i];
		if(medSetPtr->size() > maxSize) {
			maxSize = medSetPtr->size();
			maxIdx = i;
		}
	}
	medSetPtr = (*medPoints)[maxIdx];
	std::vector<geometry_msgs::Point> result(0);
	for(int i = 0; i < medSetPtr->size(); i++) {
		geometry_msgs::Point temp;
		temp.x = (*medSetPtr)[i]->x;
		temp.y = (*medSetPtr)[i]->y;
		temp.z = (*medSetPtr)[i]->z;
		result.push_back(temp);
	}
	// clean up
	for(int i = 0; i < medPoints->size(); i++) {
		medSetPtr = (*medPoints)[i];
		for(int j = 0; j < medSetPtr->size(); j++) {
			delete (*medSetPtr)[j];
		}
		delete medSetPtr;
	}
	delete medPoints;

	return result;
}

std::vector<Point*>* OpticalScanner::getYMedians(PointCloud::Ptr pcPtr) {
	std::vector<Point*>* medians = new std::vector<Point*>();

	std::vector<std::vector<Point*>*>* medPoints = new std::vector<std::vector<Point*>*>();
	std::vector<Point*>* medSetPtr;
	for(int i = 0; i < pcPtr->points.size(); i++) {
		int idx = -1;
		// Find idx
		for(int j = medPoints->size()-1; j >= 0; j--) {
			medSetPtr = (*medPoints)[j];
			if(medSetPtr->size() > 0 && *reinterpret_cast<uint32_t*>(&((*medSetPtr)[0]->rgb)) == *reinterpret_cast<uint32_t*>(&(pcPtr->points[i].rgb))) {
				idx = j;
				break;
			}
		}
		// Idx not found, create new one
		if(idx == -1) {
			medPoints->push_back(new std::vector<Point*>());
			idx = medPoints->size() - 1;
		}
		
		Point * point = new Point();
		point->x = pcPtr->points[i].x;
		point->y = pcPtr->points[i].y;
		point->z = pcPtr->points[i].z;
		point->rgb = pcPtr->points[i].rgb;
		
		medSetPtr = (*medPoints)[idx];
		bool insert = true;
		std::vector<Point*>::iterator it = medSetPtr->end();
		for(int j = medSetPtr->size()-1; j >= 0; j--) {
			if(std::abs(pcPtr->points[i].y - (*medSetPtr)[j]->y) < 0.00001) {
				insert = false;
				break;
			}
			if(pcPtr->points[i].y > (*medSetPtr)[j]->y) {
				
				medSetPtr->insert(it-(medSetPtr->size()-1 - j),point);
				insert = false;
				break;
			}
		}
		// Either there are no points in medSetPtr or point is the smallest value
		if(insert) {
			it = medSetPtr->begin();
			medSetPtr->insert(it,point);
		}
		
	}
	
	for(int i = 0; i < medPoints->size(); i++) {
		Point* median = new Point();
		Point* tempPtr1 = (*((*medPoints)[i]))[0];
		Point* tempPtr2 = (*((*medPoints)[i]))[(*medPoints)[i]->size()-1];
//		for(int j = 0; j < (*medPoints)[i]->size(); j++) {
//			ROS_ERROR("Point %d, (*(*medPoints)[%d])[%d]->y = %f", j,i,j,(*(*medPoints)[i])[j]->y);
//		}

		median->x = tempPtr1->x;
		median->y = (tempPtr1->y+tempPtr2->y)/2.0;
		median->z = tempPtr1->z;
		median->rgb = tempPtr1->rgb;
		medians->push_back(median);
//		ROS_ERROR("Point %d is a median", (*medPoints)[i]->size()/2);
	}
	// clean up
	for(int i = 0; i < medians->size(); i++) {
		medSetPtr = (*medPoints)[i];
		for(int j = 0; j < medSetPtr->size(); j++) {
			delete (*medSetPtr)[j];
		}
		delete medSetPtr;
	}
	delete medPoints;

	return medians;
}

std::vector<Point*>* OpticalScanner::getMeans(PointCloud::Ptr pcPtr) {

	std::vector<Point*>* means = new std::vector<Point*>();
	std::vector<float>* meanCounts = new std::vector<float>();

	for(int i = 0; i < pcPtr->points.size(); i++) {
		// Get index
		int idx = -1;
		for(int j = means->size()-1; j >= 0; j--) {
			if(*reinterpret_cast<uint32_t*>(&(pcPtr->points[i].rgb)) == *reinterpret_cast<uint32_t*>(&((*means)[j]->rgb))) {
				idx = j;
				break;
			}
		}
		
		if(idx == -1) {
			Point* point = new Point();
			point->x = 0.0;
			point->y = 0.0;
			point->z = 0.0;
			point->rgb = pcPtr->points[i].rgb;
			means->push_back(point);
			meanCounts->push_back(0.0);
			idx = means->size()-1;
		}
		(*means)[idx]->x += pcPtr->points[i].x;
		(*means)[idx]->y += pcPtr->points[i].y;
		(*means)[idx]->z += pcPtr->points[i].z;
		(*meanCounts)[idx] += 1.0;
	}
	for(int i = 0; i < means->size(); i++) {
		(*means)[i]->x = (*means)[i]->x / (*meanCounts)[i];
		(*means)[i]->y = (*means)[i]->y / (*meanCounts)[i];
		(*means)[i]->z = (*means)[i]->z / (*meanCounts)[i];
	} 
	delete meanCounts;
	return means;
}	

// Remove spin when using computeCartesianPath
void OpticalScanner::RemoveSpin(moveit_msgs::RobotTrajectory* traj) {
	int nJoints = traj->joint_trajectory.joint_names.size();
	int nPoints = traj->joint_trajectory.points.size();

	bool cancelForearmRoll = false;
//	bool cancelWristRoll = false;
	for(int i = 0; i < nPoints; i++) {
		if(std::abs(traj->joint_trajectory.points[i].positions[4]-traj->joint_trajectory.points[0].positions[4]) > 3.14) {
			cancelForearmRoll = true;
		}
//		if(std::abs(traj->joint_trajectory.points[i].positions[6]-traj->joint_trajectory.points[0].positions[6]) > 3.14) {
//			cancelWristRoll = true;
//		}

	}

	if(cancelForearmRoll) {
		for(int i = 0; i < nPoints; i++) {
			traj->joint_trajectory.points[i].positions[4] = traj->joint_trajectory.points[0].positions[4];
		}
	}

//	if(cancelWristRoll) {
//		for(int i = 0; i < nPoints; i++) {
//			traj->joint_trajectory.points[i].positions[6] = traj->joint_trajectory.points[0].positions[6];
//		}
//	}

}

// Scale the velocity of a trajectory
// Derived from code by Patrick Goebel (https://groups.google.com/forum/#!topic/moveit-users/7n45S8DUjys)
moveit_msgs::RobotTrajectory OpticalScanner::scaleTrajectorySpeed(moveit_msgs::RobotTrajectory* traj, double scale) {
	if(scale >= 1.0) {
		return *traj;
	}

	int nJoints = traj->joint_trajectory.joint_names.size();
	int nPoints = traj->joint_trajectory.points.size();
	std::vector<trajectory_msgs::JointTrajectoryPoint>* points = &traj->joint_trajectory.points;

	for(int i = 0; i < nPoints; i++) {
		(*points)[i].time_from_start *= (1.0/scale);

		for(int j = 0; j < nJoints; j++) {
			(*points)[i].velocities[j] *= scale;
			(*points)[i].accelerations[j] *= (scale*scale);
		}
	}
	
	return *traj;

}
