#include <stdlib.h>
#include "ros/ros.h"
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Point.h>

void drawPoint(float x, float y, float z);

ros::Publisher* pubPtr;
visualization_msgs::Marker* markerPtr;
int pointIdx;

void vizCallback(const pr2_pretouch_optical_dist::OpticalDist msg) {
	float x = msg.frontTipPos.x;
	float y = msg.frontTipPos.y;
	float z = msg.frontTipPos.z;	
	
	if(msg.frontTipData[0] < 255) {
		drawPoint(x,y,z);
	}
}

void drawPoint(float x, float y, float z) {
	
	markerPtr->header.stamp = ros::Time();
	markerPtr->points[pointIdx].x = x;
	markerPtr->points[pointIdx].y = y;
	markerPtr->points[pointIdx].z = z;
	pointIdx = (pointIdx + 1) % 5000;
	pubPtr->publish(*markerPtr);
}

int main(int argc,char ** argv) {

	ros::init(argc, argv, "optical_rviz_display");
	ros::NodeHandle node;
	std::string side = argv[1];

	if(side.compare("right") && side.compare("left")) {
		ROS_INFO("'side' param value not valid, must be 'left' or 'right'");
		ros::shutdown();
	}
	std::string pubTopic = "visualization_marker";
	std::string subTopic = "optical/"+side;
	
	visualization_msgs::Marker marker;
	marker.header.frame_id = "base_link";
	marker.ns = "optical_viz_ns";
	marker.type = visualization_msgs::Marker::SPHERE_LIST;
	marker.action = visualization_msgs::Marker::MODIFY;
	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
	marker.scale.x = 0.01;
	marker.scale.y = 0.01;
	marker.scale.z = 0.01;
	marker.color.a = 1.0;
	marker.color.r = 0.0;
	marker.color.g = 1.0;
	marker.color.b = 0.0;
	geometry_msgs::Point points[5000];
	for(int i = 0; i < 5000; i++) {
		geometry_msgs::Point point;
		point.x = 0.0;
		point.y = 0.0;
		point.z = 0.0;
		marker.points.push_back(point);
	}
	markerPtr = &marker;
	
	pointIdx = 0;
	ROS_INFO("Started publisher for visualization of %s gripper", argv[1]);
	ros::Publisher pub = node.advertise<visualization_msgs::Marker>( pubTopic, 1000 );
	pubPtr = &pub;
	ros::Subscriber sub = node.subscribe(subTopic, 1000, vizCallback);

	ros::spin();
	return 0;
}
