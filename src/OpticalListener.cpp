#include <pr2_pretouch_optical_dist/OpticalListener.h>

OpticalListener::OpticalListener(ros::NodeHandle* node, std::string side) {

	thresholds = new std::vector<int>(6);
	colors = new std::vector<uint32_t>(6);
	for(int i = 0; i < thresholds->size(); i++) {
		(*thresholds)[i] = -1;
		genRandColor(i);
	}
	nodePtr = node;
	isPaused = false;
	subTopic = "optical/"+side;
	pubTopic = "optical/point_cloud/"+side;
	pub = node->advertise<PointCloud>(pubTopic, 1000);
	
}

void OpticalListener::genRandColor(int idx) {
	(*colors)[idx] = ((uint32_t)(rand() % 256)) << 16 | ((uint32_t)(rand() % 256)) << 8 | (uint32_t)(rand() % 256);
}

OpticalListener::~OpticalListener() {
	delete thresholds;
	delete colors;
}

void OpticalListener::dataCallback(const pr2_pretouch_optical_dist::OpticalDist msg) {

	sensor_msgs::PointCloud2 pc2;
	pc2.header.frame_id = "base_link";
	pc2.header.stamp = ros::Time::now();
	pcPtr->header = pcl_conversions::toPCL(pc2.header);

	if(msg.centerData[0] < (*thresholds)[0]) {
		Point point;
		point.x = msg.centerPos.x;
		point.y = msg.centerPos.y;
		point.z = msg.centerPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[0]));
		pcPtr->points.push_back(point);
		pcPtr->width += 1;
	} else {
		genRandColor(0);
	}

	if(msg.leftBackLateralData[0] < (*thresholds)[1]) {
		Point point;
		point.x = msg.leftBackLateralPos.x;
		point.y = msg.leftBackLateralPos.y;
		point.z = msg.leftBackLateralPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[1]));
		pcPtr->points.push_back(point);
		pcPtr->width += 1;	
	} else {
		genRandColor(1);
	}	

	if(msg.leftFrontLateralData[0] < (*thresholds)[2]) {
		Point point;
		point.x = msg.leftFrontLateralPos.x;
		point.y = msg.leftFrontLateralPos.y;
		point.z = msg.leftFrontLateralPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[2]));
		pcPtr->points.push_back(point);	
		pcPtr->width += 1;
	} else {
		genRandColor(2);
	}

	if(msg.frontTipData[0] < (*thresholds)[3]) {
		Point point;
		point.x = msg.frontTipPos.x;
		point.y = msg.frontTipPos.y;
		point.z = msg.frontTipPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[3]));
		pcPtr->points.push_back(point);	
		pcPtr->width += 1;	
		
	} else {
		genRandColor(3);
	}

	if(msg.rightBackLateralData[0] < (*thresholds)[4]) {
		Point point;
		point.x = msg.rightBackLateralPos.x;
		point.y = msg.rightBackLateralPos.y;
		point.z = msg.rightBackLateralPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[4]));
		pcPtr->points.push_back(point);	
		pcPtr->width += 1;
	} else {
		genRandColor(4);
	}

	if(msg.rightFrontLateralData[0] < (*thresholds)[5]) {
		Point point;
		point.x = msg.rightFrontLateralPos.x;
		point.y = msg.rightFrontLateralPos.y;
		point.z = msg.rightFrontLateralPos.z;
		point.rgb = *reinterpret_cast<float*>(&((*colors)[5]));
		pcPtr->points.push_back(point);	
		pcPtr->width += 1;
	} else {
		genRandColor(5);
	}

	pub.publish(pcPtr);

}

void OpticalListener::start() {

	if(!isPaused) {
//		pcPtr = new sensor_msgs::PointCloud();
		PointCloud::Ptr pc(new PointCloud);
		pcPtr = pc;
		pcPtr->height = 1;
		pcPtr->width = 0;
		
//		pcPtr->header.frame_id = "base_link";
	}
	isPaused = false;
	sub = nodePtr->subscribe(subTopic, 1000, &OpticalListener::dataCallback, this);
}

void OpticalListener::pause() {
	sub.shutdown();
	isPaused = true;
}

void OpticalListener::stop() {
	sub.shutdown();
	isPaused = false;
}

PointCloud::Ptr OpticalListener::getPointCloud(std::string frameId) {
	PointCloud::Ptr newPcPtr(new PointCloud);
	newPcPtr->height = 1;
	newPcPtr->width = 0;
	tf::TransformListener tfListener;
	geometry_msgs::PointStamped pointIn;
	pointIn.header.stamp = ros::Time(0);
	pointIn.header.frame_id = "base_link";

	geometry_msgs::PointStamped pointOut;
	pointOut.header.stamp = ros::Time(0);
	pointOut.header.frame_id = frameId;
	tfListener.waitForTransform( "base_link", frameId, ros::Time::now(), ros::Duration(1.0));
	for(int i = 0; i < pcPtr->points.size();i++) {
		pointIn.point.x = pcPtr->points[i].x;
		pointIn.point.y = pcPtr->points[i].y;
		pointIn.point.z = pcPtr->points[i].z;
		try{
			tfListener.transformPoint (frameId, pointIn,pointOut);
		}catch(tf::TransformException ex) {
			ROS_ERROR("%s", ex.what());
			ros::Duration(1.0).sleep();
		}

		Point point;
		point.x = pointOut.point.x;
		point.y = pointOut.point.y;
		point.z = pointOut.point.z;
		point.rgb = pcPtr->points[i].rgb;
		newPcPtr->points.push_back(point);	
		newPcPtr->width += 1;	

	}
	return newPcPtr;
}

void OpticalListener::setThresholds(std::vector<int>* newThresholds) {
	
	if(newThresholds->size() != 6) {
		ROS_INFO("Must pass vector of size 6 to OpticalListener's setThresholds()");
		return;
	} 
	for(int i = 0; i < thresholds->size(); i++) {
		(*thresholds)[i] = (*newThresholds)[i];
	}
	
}

void OpticalListener::centerActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[0] = -1;
	} else {
		(*thresholds)[0] = threshold;
	}
}

void OpticalListener::leftBackLateralActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[1] = -1;
	} else {
		(*thresholds)[1] = threshold;
	}
}

void OpticalListener::leftFrontLateralActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[2] = -1;
	} else {
		(*thresholds)[2] = threshold;
	}
}

void OpticalListener::frontTipActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[3] = -1;
	} else {
		(*thresholds)[3] = threshold;
	}
}

void OpticalListener::rightBackLateralActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[4] = -1;
	} else {
		(*thresholds)[4] = threshold;
	}
}

void OpticalListener::rightFrontLateralActive(bool on, int threshold) {
	if(!on) {
		(*thresholds)[5] = -1;
	} else {
		(*thresholds)[5] = threshold;
	}
}
