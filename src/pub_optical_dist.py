#!/usr/bin/env python
# Created by Patrick Lancaster (planc509@cs.washington.edu)
# Derived from pub_optical.py (By Liang-Ting Jiang jianglt@uw.edu)
# Publishes data recieved by gripper

import rospy
from std_msgs.msg import ByteMultiArray
from pr2_pretouch_optical_dist.msg import OpticalDist

data = OpticalDist()
ts = [0,0,0,0]
seq = 0

rospy.init_node("optical_distance_publisher", anonymous=True)

# Get the current side and number of sensors
side = rospy.get_param('~side', 'right')
n_sense = int(rospy.get_param('~n_sense',"2"))
rospy.loginfo("Started publisher for the %s gripper", side)

if side == 'right':
	sub_topic = 'raw_pressure/r_gripper_motor'
elif side == 'left':
	sub_topic = 'raw_pressure/l_gripper_motor'
else:
	print side + ' is not a valid side parameter'
	raise

# Create publisher
pub_topic = 'optical/' + side
pub = rospy.Publisher(pub_topic, OpticalDist)

# Stor will hold most recent readings
stor = [0] * (3*n_sense)
def rawDataCallback(msg):
	global  stor,data,seq,ts
	if msg.data[:4] != ts and msg.data[4] != 0 and msg.data[4] == msg.data[5]:
		ts = msg.data[:4]
		stor[3*(msg.data[4] - 1) : 3*(msg.data[4])] = [(d + 256 if d < 0 else d) for d in msg.data[6:9]]
#		print stor
 
		data.header.seq = seq
		data.header.stamp = rospy.Time.now()

		seq += 1

		# Populate each field of message
		data.center = [0] * 2
		data.center[0] = stor[0]
		data.center[1] = stor[1] << 8 | stor[2]

		data.left_back_lateral = [0] * 2
		data.left_back_lateral[0] = stor[3]
		data.left_back_lateral[1] = stor[4] << 8 | stor[5]

		data.left_front_lateral = [0] * 2
		data.left_front_lateral[0] = stor[6]
		data.left_front_lateral[1] = stor[7] << 8 | stor[8]

		data.front_tip = [0] * 2
		data.front_tip[0] = stor[9]
		data.front_tip[1] = stor[10] << 8 | stor[11]

		data.right_back_lateral = [0] * 2
		data.right_back_lateral[0] = stor[12]
		data.right_back_lateral[1] = stor[13] << 8 | stor[14]

		data.right_front_lateral = [0] * 2
		data.right_front_lateral[0] = stor[15]
		data.right_front_lateral[1] = stor[16] << 8 | stor[17]

		pub.publish(data)

# Subscribe to topic published by gripper
sub = rospy.Subscriber(sub_topic, ByteMultiArray, rawDataCallback)
rospy.spin()
