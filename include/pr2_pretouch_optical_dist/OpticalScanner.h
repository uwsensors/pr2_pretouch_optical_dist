#ifndef OPTICAL_SCAN_H
#define OPTICAL_SCAN_H

#include <string>
#include <math.h>
#include <pr2_pretouch_optical_dist/OpticalListener.h>
#include <moveit/move_group_interface/move_group.h>
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include <tf/transform_listener.h>
#include <pr2_pretouch_optical_dist/Gripper.h>
#include <pr2_controllers_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;
typedef pcl::PointXYZRGB Point;
typedef actionlib::SimpleActionClient< pr2_controllers_msgs::JointTrajectoryAction > TrajClient;

class OpticalScanner{

	private: moveit::planning_interface::MoveGroup* groupPtr;
		OpticalListener* optListenPtr;
		std::string side;
		tf::TransformListener* tfListenerPtr;
		TrajClient* trajClient;
		int gripJointIdx;
		double curGripWidth;
		// Scale the velocity of a trajectory
		// Derived from code by Patrick Goebel (https://groups.google.com/forum/#!topic/moveit-users/7n45S8DUjys)
		moveit_msgs::RobotTrajectory scaleTrajectorySpeed(moveit_msgs::RobotTrajectory* traj, double scale);
		// Helper method for when an object is initially detected
		geometry_msgs::PoseStamped initDetectScan(double* threshold, int n, double offset);
		// Helper method for when an object is not initially detected
		geometry_msgs::PoseStamped initNoDetectScan(double* threshold, int n, double offset);
		// Given a point cloud, return a list of points (ordered by y coordinate) that belong to the most scanned object
		std::vector<geometry_msgs::Point> getOrderedScanPoints(PointCloud::Ptr pcPtr); 
		// Gets the average value of the front sensor over n samples and adds the passed offset to get the returned threshold
		double getThreshold(int n, double offset);
		void subCallback(const sensor_msgs::JointState msg);
		
		geometry_msgs::Point findEdge(Gripper* gripper, int initRead, int* finalRead, double threshold, double effort);
		void spinGripper(double angle, double duration, bool async);

	public: 
		OpticalScanner(ros::NodeHandle* node,  std::string& group, std::string& side, std::vector<int>* thresholds=NULL);
		~OpticalScanner();
		// Aligns the fingertip with an edge and then performs a scan
		std::vector<geometry_msgs::Point> horizontalScan(double velScale=1.0);
		// Moves the gripper towards the goal position until something is detected between the fingertips
		bool graspDetect(geometry_msgs::PoseStamped goal, double threshold=-1.0);
		// Moves to the goals passed in by waypoints
		geometry_msgs::PoseStamped waypointsMove(std::vector<geometry_msgs::PoseStamped> wayPoints, bool async, double velScale=1.0, bool collisionCheck=false);
		// Remove spin when using computeCartesianPath
		void RemoveSpin(moveit_msgs::RobotTrajectory* traj);
		std::vector<Point*>* getYMedians(PointCloud::Ptr pcPtr);
		std::vector<Point*>* getMeans(PointCloud::Ptr pcPtr);	
		geometry_msgs::Quaternion getAlignOrientation(std::vector<double> x, std::vector<double> y);	
		geometry_msgs::PoseStamped getCurrentPose();
		std::string getPoseReferenceFrame();
		std::string getEndEffectorLink();
		std::string getPlanningFrame();
		geometry_msgs::Point edgeScan();
		
};

#endif 
