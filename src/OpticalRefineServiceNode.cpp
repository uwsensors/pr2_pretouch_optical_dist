#include "ros/ros.h"
#include "pr2_pretouch_optical_dist/OpticalRefineService.h"
#include <stdio.h>
#include <stdlib.h>

// Create Optical Refine Object that advertises refinement services and spin
int main(int argc, char** argv) {

	ros::init(argc, argv, "optical_refine_service_node");
	ros::NodeHandle node;

	OpticalRefine* optRef = new OpticalRefine(&node);
	ros::spin();
//
}
/*
int main(int argc, char** argv) {

	ros::init(argc,argv, "optical_refine_service_client");
	ros::NodeHandle node;
	ros::ServiceClient client = node.serviceClient<pr2_optical_grasp::MoveArmIk>("move_arm_ik");
	pr2_optical_grasp::MoveArmIk srv;
	srv.request.arm = pr2_optical_grasp::MoveArmIkRequest::RIGHT_ARM;
	geometry_msgs::PoseStamped pose;
	pose.header.frame_id = "r_wrist_roll_link";
	pose.pose.position.x = 0.2;
	pose.pose.position.y = 0.0;
	pose.pose.position.z = 0.0;
	pose.pose.orientation.w = 1.0;
	srv.request.goal = pose;

	if(client.call(srv)) {
		ROS_INFO("SUCCESS");
	} else {
		ROS_INFO("FAILURE");
	}

	return 0;

	
}*/
