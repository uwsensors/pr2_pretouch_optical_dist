#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include "pr2_pretouch_msgs/GetProbabilisticPointCloud.h"
#include "geometry_msgs/PoseStamped.h"
#include <moveit/move_group_interface/move_group.h>
#include <tf/transform_listener.h>
#include "pr2_pretouch_optical_dist/OpticalRefine.h"
#include <pr2_pretouch_optical_dist/Gripper.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/Eigenvalues>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

static const std::string KINECT_CLOUD_TOPIC = "optical_kinect_cloud";
static const std::string KINECT_PROB_CLOUD_TOPIC = "optical_prob_kinect_cloud";
static const std::string KINECT_POSE_TOPIC = "optical_grip_pose";

geometry_msgs::PoseStamped getPoseFromCloud(sensor_msgs::PointCloud cloud) {
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcPtr(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcPtr->height = 1;
  pcPtr->width = 0;

  float zMax = cloud.points[0].z;
  float zMin = cloud.points[0].z;

  for(int i = 0; i<cloud.points.size(); i++) {
    pcl::PointXYZRGB point;
    point.x = cloud.points[i].x;
    point.y = cloud.points[i].y;
    point.z = 0.0;
    pcPtr->push_back(point);
    pcPtr->width++;
    if(cloud.points[i].z < zMin) {
      zMin = cloud.points[i].z;
    }else if(cloud.points[i].z > zMax) {
      zMax = cloud.points[i].z;
    }
  }

  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*pcPtr, centroid);
  Eigen::Matrix3f covariance;
  pcl::computeCovarianceMatrixNormalized(*pcPtr, centroid, covariance);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigenSolver(covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix3f eigVecs = eigenSolver.eigenvectors();
  eigVecs.col(2) = eigVecs.col(0).cross(eigVecs.col(1));

  Eigen::Matrix4f transform(Eigen::Matrix4f::Identity());
  transform.block<3,3>(0,0) = eigVecs.transpose();
  transform.block<3,1>(0,3) =  -1.f * (transform.block<3,3>(0,0) * centroid.head<3>());
  pcl::PointCloud<pcl::PointXYZRGB> cPoints;
  pcl::transformPointCloud(*pcPtr, cPoints, transform);

  pcl::PointXYZRGB minPt, maxPt;
  pcl::getMinMax3D(cPoints, minPt, maxPt);
  const Eigen::Vector3f meanDiag = 0.5f*(maxPt.getVector3fMap() + minPt.getVector3fMap());

  const Eigen::Quaternionf quat(eigVecs);
  const Eigen::Vector3f trans = eigVecs*meanDiag + centroid.head<3>();
  Eigen::Vector4f coeffs = quat.coeffs();

	tf::Quaternion yQuat(0.0, 1.0,0.0,0.0); // For top grasp
  tf::Quaternion tfQuat(coeffs(0),coeffs(1),coeffs(2),coeffs(3));
  geometry_msgs::Quaternion finalQuat;
  tfQuat = tfQuat*yQuat; // rotate 180 degrees about y-axis
  tf::quaternionTFToMsg(tfQuat,finalQuat);

  geometry_msgs::PoseStamped gripPose;
  gripPose.header.frame_id = "base_link";
  gripPose.header.stamp = ros::Time(0);
	gripPose.pose.position.x = trans(0)+0.02;
	gripPose.pose.position.y = trans(1);
	gripPose.pose.position.z = zMax + 0.24;
  gripPose.pose.orientation = finalQuat;

	return gripPose;
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "optical_edge_grasp");
	ros::NodeHandle node;

	ros::AsyncSpinner spinner(1);
	spinner.start();
	Gripper gripper("right");
	gripper.open(-1.0, true);
	moveit::planning_interface::MoveGroup group("right_arm");
//	double temp[7] = {-0.925614,-0.077037,-1.620044,-1.028569,-18.756835,-0.097424,-17.302886};
//	double temp[7] = {-0.8,-0.5,0.0,-1.5707,0.0,0.0,0.0};
	double temp[7] = {-0.9365,-0.35,-1.3353,-0.805,0.0,0.0,0.0};
	std::vector<double> jointVals(&temp[0], &temp[0]+7);
	group.setJointValueTarget(jointVals);
	group.move();
	spinner.stop();
	ros::ServiceClient requestPCClient = node.serviceClient<std_srvs::Empty>("/tabletop_octomap");
	ros::ServiceClient getPcClient = node.serviceClient<pr2_pretouch_msgs::GetProbabilisticPointCloud>("/get_probabilistic_pointcloud");
	ros::ServiceClient moveClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_move");
	std_srvs::Empty emptySrv;
	ros::service::waitForService("/tabletop_octomap");
	if(requestPCClient.call(emptySrv)) {
		ROS_INFO("tabletop_octomap success");
	} else {
		ROS_INFO("tabletop_octomap failed");
	}

	pr2_pretouch_msgs::GetProbabilisticPointCloud pcSrv;
	ros::service::waitForService("/get_probabilistic_pointcloud");
	if(getPcClient.call(pcSrv)) {
		ROS_INFO("get_probabilistic_pointcloud success");
	} else {
		ROS_INFO("get_probabilistic_pointcloud failed");
	}

	ros::Publisher pubCloud = node.advertise<sensor_msgs::PointCloud>(KINECT_CLOUD_TOPIC,1000);
	ros::Publisher pubProbCloud = node.advertise<sensor_msgs::PointCloud>(KINECT_PROB_CLOUD_TOPIC,1000);
	pcSrv.response.original_cloud.header.frame_id = "base_link";
	pcSrv.response.cloud.header.frame_id = "base_link";
	pubProbCloud.publish(pcSrv.response.cloud);
	pubCloud.publish(pcSrv.response.original_cloud);
	
	ros::Publisher posePub = node.advertise<geometry_msgs::PoseStamped>(KINECT_POSE_TOPIC, 1);
	geometry_msgs::PoseStamped gripPose = getPoseFromCloud(pcSrv.response.original_cloud);
	
	posePub.publish(gripPose);
	ros::Duration(2.0).sleep();

	pr2_pretouch_optical_dist::OpticalRefine moveSrv;
	moveSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
	moveSrv.request.goal = gripPose;
	moveSrv.request.collision_check = 1;
	ROS_INFO("ABOUT TO MOVE TO PREGRASP");	
	ros::service::waitForService("optical_move");
	ROS_ERROR("ABOUT TO MOVE");
	if(moveClient.call(moveSrv)) {
		ROS_INFO("SUCCESSFUL move");
	} else {
		ROS_INFO("BAD move");
	}

	ROS_INFO("ABOUT TO DO EDGE SCAN");
	pr2_pretouch_optical_dist::OpticalRefine scanSrv;
	scanSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
	ros::ServiceClient scanClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_edge_scan");
	// Use optical scan service
	ros::service::waitForService("optical_edge_scan");
	if(scanClient.call(scanSrv)) {
		ROS_INFO("SUCCESSFUL SCAN");
	} else {
		ROS_INFO("BAD SCAN");
	}

	ROS_INFO("ABOUT TO GO IN FOR GRASP");
	ros::ServiceClient grabClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_detect_grasp");
	geometry_msgs::PoseStamped goal;
	goal.header.stamp = ros::Time(0);
	goal.header.frame_id = "r_wrist_roll_link";
	goal.pose.position.x = scanSrv.response.scan[0].x + 0.03; 
	goal.pose.position.y = scanSrv.response.scan[0].y;
	goal.pose.position.z = 0.0;
	goal.pose.orientation.x = 0.0;
	goal.pose.orientation.y = 0.0;
	goal.pose.orientation.z = 0.0;
	goal.pose.orientation.w = 1.0;
	scanSrv.request.goal = goal;

	ros::service::waitForService("optical_detect_grasp");
	if(grabClient.call(scanSrv)) {
		ROS_INFO("SUCCESSFUL Detect");
	} else {
		ROS_INFO("BAD Detect");
	}
	gripper.close(25.0,false);

	geometry_msgs::PoseStamped lift;
	lift.header.stamp = ros::Time(0);
	lift.header.frame_id = "r_wrist_roll_link";
	lift.pose.position.x = -0.03;
	lift.pose.position.y = 0.0;
	lift.pose.position.z = 0.0;
	lift.pose.orientation.x = 0.0;
	lift.pose.orientation.y = 0.0;
	lift.pose.orientation.z = 0.0;
	lift.pose.orientation.w = 1.0;
	moveSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
	moveSrv.request.goal = lift;

	ros::service::waitForService("optical_move");
	ROS_INFO("ABOUT TO LIFT");
	if(moveClient.call(moveSrv)) {
		ROS_INFO("SUCCESSFUL move");
	} else {
		ROS_INFO("BAD move");
	}
	ROS_ERROR("DONE WITH MOVE");

	std::string release;
	std::cin >> release;

	gripper.open();	
	spinner.start();
	group.setJointValueTarget(jointVals);
	group.move();
	spinner.stop();
	while(ros::ok()) {
		pubProbCloud.publish(pcSrv.response.cloud);
		pubCloud.publish(pcSrv.response.original_cloud);
		posePub.publish(gripPose);
		ros::Duration(1.0).sleep();
	}
}
