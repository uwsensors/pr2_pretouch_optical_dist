#include <stdlib.h>
#include "ros/ros.h"
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include <geometry_msgs/Point.h>
#include <sensor_msgs/PointCloud.h>
#include <pr2_pretouch_optical_dist/OpticalGMM.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <math.h>
ros::Publisher* pubPtr;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcPtr;
//OpticalGMM* gmmPtr;
ros::Subscriber* subPtr;

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;
typedef pcl::PointXYZRGB Point;

void pcCallback(const pr2_pretouch_optical_dist::OpticalDist msg) {

//	if(pcPtr->points.size() == 400 && pcPtr->points.size() > 0) {
//		ROS_INFO("About to cluster");
//		gmmPtr->GMMsolve(pcPtr, 3, false, true, false);
//		ROS_INFO("About to print from callback");
//		std::vector<int> counts(3);
//		counts[0] = 0;
//		counts[1] = 0;
//		counts[2] = 0;

//		for(int i = 0; i < pcPtr->channels[0].values.size(); i++) {
//			ROS_INFO("%d val = %f", i, pcPtr->channels[0].values[i]);
//			counts[(int)pcPtr->channels[0].values[i]]++;
//		}
		
//		ROS_INFO("%d %d %d",counts[0],counts[1],counts[2]);
//		ROS_INFO("Done printing from callback");
//		subPtr->shutdown();
//	}
	sensor_msgs::PointCloud2 pc2;
	pc2.header.frame_id = "base_link";
	pc2.header.stamp = ros::Time::now();
	pcPtr->header = pcl_conversions::toPCL(pc2.header);
/*	if(msg.centerData[0] < 255) {
		geometry_msgs::Point32 point;
		point.x = msg.centerPos.x;
		point.y = msg.centerPos.y;
		point.z = msg.centerPos.z;
		pcPtr->points.push_back(point);	
	} */
/*
	if(msg.leftBackLateralData[0] < 255) {
		geometry_msgs::Point32 point;
		point.x = msg.leftBackLateralPos.x;
		point.y = msg.leftBackLateralPos.y;
		point.z = msg.leftBackLateralPos.z;
		pcPtr->points.push_back(point);	
	}	

	if(msg.leftFrontLateralData[0] < 255) {
		geometry_msgs::Point32 point;
		point.x = msg.leftFrontLateralPos.x;
		point.y = msg.leftFrontLateralPos.y;
		point.z = msg.leftFrontLateralPos.z;
		pcPtr->points.push_back(point);	
	}
*/
	if(msg.frontTipData[0] < 255) {
		Point point;
		point.x = msg.frontTipPos.x;
		point.y = msg.frontTipPos.y;
		point.z = msg.frontTipPos.z;
		pcPtr->points.push_back(point);
		pcPtr->width += 1;
	/*	if(pcPtr->points.size() > 300) {
			ROS_INFO("About to do prediction");
			std::vector<geometry_msgs::Point32> points(1);
			points[1] = point;
			int idx = (gmmPtr->predict(points))[0];
			ROS_INFO("Got prediction back");
			pcPtr->channels[0].values.push_back(idx);
			ROS_INFO("Did prediction");
		}		*/
//		pcPtr->channels.push_back(channel); 
		
	}
/*
	if(msg.rightBackLateralData[0] < 255) {
		geometry_msgs::Point32 point;
		point.x = msg.rightBackLateralPos.x;
		point.y = msg.rightBackLateralPos.y;
		point.z = msg.rightBackLateralPos.z;
		pcPtr->points.push_back(point);	
	}

	if(msg.rightFrontLateralData[0] < 255) {
		geometry_msgs::Point32 point;
		point.x = msg.rightFrontLateralPos.x;
		point.y = msg.rightFrontLateralPos.y;
		point.z = msg.rightFrontLateralPos.z;
		pcPtr->points.push_back(point);	
	}
*/	
	pubPtr->publish(pcPtr);
}


int main(int argc,char ** argv) {

	ros::init(argc, argv, "optical_point_cloud_publisher");
	ros::NodeHandle node;
	ros::Duration(5.0).sleep();
	std::string side = argv[1];

//	gmmPtr = new OpticalGMM();
	if(side.compare("right") && side.compare("left")) {
		ROS_INFO("'side' param value not valid, must be 'left' or 'right'");
		ros::shutdown();
	}
	std::string pubTopic = "optical/point_cloud/"+side;
	std::string subTopic = "optical/"+side;
	PointCloud::Ptr pc(new PointCloud);

//	sensor_msgs::PointCloud pc;
	pcPtr = pc;
//	pcPtr->header.frame_id = "base_link";
	pcPtr->height = 1;
	pcPtr->width = 0;
//	pcPtr = &pc;

//	sensor_msgs::ChannelFloat32 intensity;
//	intensity.name = "intensities";
//	pcPtr->channels.push_back(intensity);
//	ROS_INFO("Channel name = %s", pcPtr->channels[0].name.c_str());
//	ROS_INFO("# of channels = %d", pcPtr->channels.size());
/*	geometry_msgs::Point points[5000];
	for(int i = 0; i < 5000; i++) {
		geometry_msgs::Point point;
		point.x = 0.0;
		point.y = 0.0;
		point.z = 0.0;
		marker.points.push_back(point);
	} */
	
	ROS_INFO("Publishing optical point cloud for %s gripper", argv[1]);
//	ros::Publisher pub = node.advertise<sensor_msgs::PointCloud>( pubTopic, 1000 );
	ros::Publisher pub = node.advertise<PointCloud>(pubTopic, 1000);
	pubPtr = &pub;
	ros::Subscriber sub = node.subscribe(subTopic, 1000, pcCallback);
	subPtr = &sub;
	ros::spin();
	return 0;
}
