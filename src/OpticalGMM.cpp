#include <pr2_pretouch_optical_dist/OpticalGMM.h>

void OpticalGMM::split(const std::string& s, char c, std::vector<std::string>& v) {
   int i = 0;
   int j = s.find(c);

   while (j != std::string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);

      if (j == std::string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

void OpticalGMM::readData(std::string inFile, int K) {

	this->K = K;
	std::ifstream file(inFile.c_str(),std::ios::in | std::ios::binary);

	std::string line;
	std::getline(file, line);

	std::vector<std::string> dims;
	std::vector<std::string> values;
	split(line, ' ', dims);
	
	xPtr = new Eigen::MatrixXd(atoi(dims[0].c_str()), atoi(dims[1].c_str()));
	std::string dataStr = "";
	int count = 0;

	while(std::getline(file, line)) {
		split(line, ' ', values);
		for(int i = 0; i < values.size(); i++) {
			(*xPtr)(count,i) = atof(values[i].c_str());
		}
		values.clear();
		count++;
	}
}

void OpticalGMM::writeData() {
	std::ofstream file;
	file.open("/home/planc509/optical_dist_ws/src/pr2_pretouch_optical_dist/src/wineResult.txt");
	for(int i = 0; i < xPtr->innerSize(); i++) {
		Eigen::MatrixXd::Index maxIdx;
		posteriorsPtr->row(i).maxCoeff(&maxIdx);
		file << xPtr->row(i) << " " << maxIdx << "\n";
	} 
/*	file << "\n\n";
	for(int i = 0; i < posteriors.innerSize(); i++) {
		file << posteriors.row(i) <<  "\n";
	} */
}

void OpticalGMM::getLikelihood(double& likelihood, bool debug) {
	if(posteriorsPtr->innerSize() != xPtr->innerSize()) {
		delete posteriorsPtr;
		posteriorsPtr = new Eigen::MatrixXd(xPtr->innerSize(), meansPtr->innerSize());
	} 

	if(debug) {
		ROS_INFO("Created new xPtr");
	}
	Eigen::MatrixXd sigInv(xPtr->outerSize(), xPtr->outerSize());
	bool invertible;
	double det; 
	Eigen::MatrixXd diff;
	likelihood = 0.0;
	
	for(int j = 0; j < meansPtr->innerSize(); j++) {
		det = 1.0;		
		
		for(int l = 0; l < sigInv.innerSize(); l++) {
			for(int m = 0; m < sigInv.outerSize(); m++) {
				if(l == m) {
					sigInv(l,m) = 1.0/(*sigPtr)(j,l);
					det *= (*sigPtr)(j,l);
				} else {
					sigInv(l,m) = 0.0;
				}
			}
		}
	
		for(int i = 0; i < xPtr->innerSize(); i++) {
			diff = xPtr->row(i) - meansPtr->row(j);

			Eigen::MatrixXd temp = -0.5*(diff*sigInv*diff.transpose());
			(*posteriorsPtr)(i,j) = temp(0,0)+log((*wPtr)(j)/sqrt(det*pow(2*M_PI,xPtr->outerSize())));

		}
	}

	Eigen::VectorXd maxPost = posteriorsPtr->rowwise().maxCoeff();
	
	for(int  i = 0; i < xPtr->innerSize(); i++) {
		likelihood += maxPost(i);
		double logSum = 0.0;
		for(int j = 0; j < meansPtr->innerSize(); j++) {
			logSum += exp((*posteriorsPtr)(i,j)-maxPost(i));
			(*posteriorsPtr)(i,j) = exp((*posteriorsPtr)(i,j));
			
		}
		likelihood += log(logSum);
	}
	
	Eigen::VectorXd sumPost = posteriorsPtr->rowwise().sum();
	for(int i = 0; i < xPtr->innerSize(); i++) {
		posteriorsPtr->row(i) = posteriorsPtr->row(i) / sumPost(i);
	}
	
}

OpticalGMM::OpticalGMM() {
	xPtr = NULL;
	posteriorsPtr = NULL;
	wPtr = NULL;
	meansPtr = NULL;
	sigPtr = NULL;
	
}

OpticalGMM::~OpticalGMM() {
	delete xPtr;
	delete posteriorsPtr;
	delete wPtr;
	delete meansPtr;
	delete sigPtr;
}

void OpticalGMM::loadFromFile() {
	int K = 3;
	readData("/home/planc509/optical_dist_ws/src/pr2_pretouch_optical_dist/src/wineTrain.txt", K);
	initParams();
	GMMHelper();
	writeData();
}

std::vector<int> OpticalGMM::predict(std::vector<geometry_msgs::Point32> points) {
	int d = 0;
	if(useX) {
		d++;
	}
	if(useY) {
		d++;
	}
	if(useZ) {
		d++;
	}
	if(xPtr != NULL) {
		delete xPtr;
	}
	xPtr = new Eigen::MatrixXd(points.size(), d);
	geometry_msgs::Point32 point;
	for(int i = 0; i < points.size(); i++) {
		point = points[i];
		int j = 0;
		if(useX) {
			(*xPtr)(i,j) = point.x;
			j++;
		}
		if(useY) {
			(*xPtr)(i,j) = point.y;
			j++;
		}
		if(useZ) {
			(*xPtr)(i,j) = point.z;
		}
	}
	double likelihood;
	ROS_INFO("going to get likelihood");
	getLikelihood(likelihood, true);
	ROS_INFO("likelihood has been retireved");
	std::vector<int> results(points.size());
	std::vector<int> counts(3);
	counts[0] = 0;
	counts[1] = 0;
	counts[2] = 0;
	for(int i = 0; i < xPtr->innerSize(); i++) {
		Eigen::MatrixXd::Index maxIdx;
		posteriorsPtr->row(i).maxCoeff(&maxIdx);
		results[i] = maxIdx;
	
	} 
	
	ROS_INFO("About to return");
	return results;
}

double OpticalGMM::GMMHelper() {

	double likelihood = 0.0;

	getLikelihood(likelihood);

	double oldLikelihood = likelihood-log(1.02);
	int count = 0;	
	Eigen::MatrixXd diff(1,xPtr->outerSize());
	Eigen::VectorXd newW(wPtr->size());
	Eigen::MatrixXd newMeans(K, xPtr->outerSize());
	while(likelihood > log(1.005) + oldLikelihood) {
		
		oldLikelihood = likelihood;
		
		newW = posteriorsPtr->colwise().sum();
		newW = newW / xPtr->innerSize();
		for(int i = 0; i < wPtr->size(); i++) {
			(*wPtr)(i) = newW(i);
		}
	
		newMeans = (posteriorsPtr->transpose()*(*xPtr)) / xPtr->innerSize();
		for(int k = 0; k < K; k++) {
			meansPtr->row(k) = newMeans.row(k)/(*wPtr)(k);
		} 
	
		for(int k = 0; k < K; k++) {
			sigPtr->row(k).setZero();
			for(int i = 0; i < xPtr->innerSize(); i++) {
				diff = xPtr->row(i)-meansPtr->row(k);

				for(int j = 0; j < xPtr->outerSize(); j++) {
					(*sigPtr)(k,j) += diff(0,j)*diff(0,j)*(*posteriorsPtr)(i,k);
				}

			} 

			sigPtr->row(k) = sigPtr->row(k) / (xPtr->innerSize()*(*wPtr)(k));
		}
		getLikelihood(likelihood);
		count++;
		ROS_INFO("%d Likelihood = %f",count, likelihood);
	}
	std::cout << *meansPtr;
	ROS_INFO("Finished clustering!");
	return likelihood;
}

void OpticalGMM::initParams() {
	if(wPtr == NULL) {
		wPtr = new Eigen::VectorXd(K);
		for(int k = 0; k < K; k++) {
			(*wPtr)(k) = 1.0;
		}
	}

	Eigen::VectorXd maxVals = xPtr->row(0);
	Eigen::VectorXd minVals = xPtr->row(0);

	for(int i = 1; i < xPtr->innerSize(); i++) {
		for(int j = 0; j < xPtr->outerSize(); j++) {
			if((*xPtr)(i,j) > maxVals(j)) {
				maxVals(j) = (*xPtr)(i,j);
			} else if((*xPtr)(i,j) < minVals(j)) {
				minVals(j) = (*xPtr)(i,j);
			}
		}
	}

	if(meansPtr == NULL) {
		Eigen::VectorXd meanRange =  maxVals - minVals;
		meansPtr = new Eigen::MatrixXd(K, xPtr->outerSize());
		for(int k = 0; k < K; k++) {
			meansPtr->row(k) = minVals + ((1.0*k) / (K-1))*meanRange;
		}
//		int idx;
//		// Pick random initial means
//		for(int k = 0; k < K; k++) {
//			idx = rand() % xPtr->innerSize();
//			meansPtr->row(k) = xPtr->row(idx);
//		}
		std::cout << *meansPtr << "\n";
	}

	if(sigPtr == NULL) {

	
		Eigen::VectorXd sigRange =  maxVals - minVals;
		sigPtr = new Eigen::MatrixXd(K, xPtr->outerSize());

		for(int k = 0; k < K; k++) {
			sigPtr->row(k) = 0.5*sigRange;
		}
	}

	if(posteriorsPtr == NULL) {
		posteriorsPtr = new Eigen::MatrixXd(xPtr->innerSize(),K);
	}
}

double OpticalGMM::GMMsolve(PointCloud::Ptr pcPtr, int K, bool useX, bool useY, bool useZ) {

	this->K = K;
	this->useX = useX;
	this->useY = useY;
	this->useZ = useZ;
	int d = 0;
	if(useX) {
		d++;
	}
	if(useY) {
		d++;
	}
	if(useZ) {
		d++;
	}
	if(xPtr != NULL) {
		delete xPtr;
	}
	xPtr = new Eigen::MatrixXd(pcPtr->points.size(), d);
	Point point;
	for(int i = 0; i < pcPtr->points.size(); i++) {
		point = pcPtr->points[i];
		int j = 0;
		if(useX) {
			(*xPtr)(i,j) = point.x;
			j++;
		}
		if(useY) {
			(*xPtr)(i,j) = point.y;
			j++;
		}
		if(useZ) {
			(*xPtr)(i,j) = point.z;
		}
	}

	initParams();

	double likelihood;
	likelihood = GMMHelper();

	Eigen::MatrixXd::Index maxIdx;
	
	std::vector<int> counts(3);
	counts[0] = 0;
	counts[1] = 0;
	counts[2] = 0;
	for(int i = 0; i < pcPtr->points.size(); i++) {
		uint32_t rgb = 255;		
		posteriorsPtr->row(i).maxCoeff(&maxIdx);
		rgb = rgb << (8 * maxIdx);
		pcPtr->points[i].rgb = *reinterpret_cast<float*>(&rgb);
		counts[maxIdx]++;
	//	pcPtr->channels[0].values.push_back(maxIdx);
	}
	ROS_INFO("COUNTS = %d, %d, %d", counts[0], counts[1], counts[2]);
	ROS_INFO("About to return from GM");
	return likelihood;

}

int main(int argc, char** argv) {

	
	OpticalGMM gmm;
	gmm.loadFromFile();
//	Eigen::MatrixXd posteriors(x.innerSize(), K);
	
//	posteriors = GMMHelper(x, K);
//	writeData(x, posteriors);
}

/*
Eigen::MatrixXd getLikelihood(Eigen::MatrixXd x, Eigen::MatrixXd means, std::vector<Eigen::MatrixXd>& sig, Eigen::MatrixXd w, double& likelihood) {
	Eigen::MatrixXd posteriors(x.innerSize(), means.innerSize());
	Eigen::MatrixXd sigInv(x.outerSize(), x.outerSize());
	bool invertible;
	double det; 
	Eigen::MatrixXd diff;
	likelihood = 0.0;
	
	for(int j = 0; j < means.innerSize(); j++) {
		det = sig[j].determinant();		
		sigInv = sig[j].inverse();
	
		for(int i = 0; i < x.innerSize(); i++) {
			diff = x.row(i) - means.row(j);

			Eigen::MatrixXd temp = -0.5*(diff*sigInv*diff.transpose());
			posteriors(i,j) = temp(0,0)+log(w(j)/sqrt(det*pow(2*M_PI,x.outerSize())));

		}
	}

	Eigen::VectorXd maxPost = posteriors.rowwise().maxCoeff();
	
	for(int  i = 0; i < x.innerSize(); i++) {
		likelihood += maxPost(i);
		double logSum = 0.0;
		for(int j = 0; j < means.innerSize(); j++) {
			logSum += exp(posteriors(i,j)-maxPost(i));
			posteriors(i,j) = exp(posteriors(i,j));
			
		}
		likelihood += log(logSum);
	}
	
	Eigen::VectorXd sumPost = posteriors.rowwise().sum();
	for(int i = 0; i < x.innerSize(); i++) {
		posteriors.row(i) = posteriors.row(i) / sumPost(i);
	}
	std::cout << posteriors;
	return posteriors;
	
}
*/
