#include "pr2_pretouch_optical_dist/OpticalRefineService.h"

OpticalRefine::OpticalRefine(ros::NodeHandle* nodePtr) {
	this->nodePtr = nodePtr;
	std::string leftGroup = "left_arm";
	std::string rightGroup = "right_arm";
	std::string leftSide = "left";
	std::string rightSide = "right";
	this->rOptScanner = new OpticalScanner(nodePtr, rightGroup, rightSide);
	this->lOptScanner = new OpticalScanner(nodePtr, leftGroup, leftSide);

	// Advertise scanning, grasping and item detection services 
	serviceHorizontalScan = nodePtr->advertiseService("optical_horizontal_scan", &OpticalRefine::horizontalScan, this);
	serviceEdgeScan = nodePtr->advertiseService("optical_edge_scan", &OpticalRefine::edgeScan, this);	
	serviceDetectGrasp = nodePtr->advertiseService("optical_detect_grasp", &OpticalRefine::graspDetect, this);
	serviceDetectItem = nodePtr->advertiseService("optical_detect_item", &OpticalRefine::itemDetect, this);
	serviceMove = nodePtr->advertiseService("optical_move", &OpticalRefine::waypointsMove, this);
}
		
OpticalRefine::~OpticalRefine() {

	delete rOptScanner;	
	delete lOptScanner;

}
	
// Moves to the goal until something is detected between the fingertips
bool OpticalRefine::graspDetect(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response) {
	OpticalScanner* optScanPtr;
	if(request.arm == pr2_pretouch_optical_dist::OpticalRefineRequest::LEFT_ARM) {
		optScanPtr = lOptScanner;
	} else {
		optScanPtr = rOptScanner;
	}
	bool success = optScanPtr->graspDetect(request.goal);
	response.success = success;
	return success;
}

// Preforms a scan of the object
bool OpticalRefine::horizontalScan(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response) {
		
	OpticalScanner* optScanPtr;
	if(request.arm == pr2_pretouch_optical_dist::OpticalRefineRequest::LEFT_ARM) {
		optScanPtr = lOptScanner;
	} else {
		optScanPtr = rOptScanner;
	}
	ROS_ERROR("ABOUT TO CALL HORIZONTAL SCAN");
	std::vector<geometry_msgs::Point> results = optScanPtr->horizontalScan(0.15);
	bool success = results.size() > 0;
	response.scan = results;
	response.success = success;
	
	return success;
}	

bool OpticalRefine::edgeScan(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response) {
	ROS_ERROR("IN EDGE SCAN");	
	OpticalScanner* optScanPtr;
	if(request.arm == pr2_pretouch_optical_dist::OpticalRefineRequest::LEFT_ARM) {
		optScanPtr = lOptScanner;
	} else {
		optScanPtr = rOptScanner;
	}
	ROS_ERROR("ABOUT TO CALL EDGE SCAN");
	geometry_msgs::Point result = optScanPtr->edgeScan();
	ROS_ERROR("RETURNED FROM EDGE SCAN");
	std::vector<geometry_msgs::Point> results;
	results.push_back(result);
	response.success = result.x != 0.0 || result.y != 0.0 || result.z != 0.0;
	response.scan = results;

	return true;
}

// Detects if an item is blocking one of the sensors (not including the center one on the pad)
bool OpticalRefine::itemDetect(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response)  {
	std::string side;	
	if(request.arm == pr2_pretouch_optical_dist::OpticalRefineRequest::LEFT_ARM) {
		side = "left";
	} else {
		side = "right";
	}
	bool detect;
	pr2_pretouch_optical_dist::OpticalDistConstPtr detectMsg = ros::topic::waitForMessage<pr2_pretouch_optical_dist::OpticalDist>("optical/"+side);
	detect = detectMsg->leftBackLateralData[0] < 100.0 || detectMsg->leftFrontLateralData[0] < 100.0 || detectMsg->frontTipData[0] < 100.0 || detectMsg->rightBackLateralData[0] < 100.0 || detectMsg->rightFrontLateralData[0] < 100.0; 
	response.detect = detect;
	return true;
}

bool OpticalRefine::waypointsMove(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response) {
	OpticalScanner* optScanPtr;
	if(request.arm == pr2_pretouch_optical_dist::OpticalRefineRequest::LEFT_ARM) {
		optScanPtr = lOptScanner;
	} else {
		optScanPtr = rOptScanner;
	}
	std::vector<geometry_msgs::PoseStamped> goals(0);
	goals.push_back(request.goal);
	bool collisionCheck = request.collision_check == 1;
	optScanPtr->waypointsMove(goals, false, 1.0, collisionCheck);
	return true; 
	
}
