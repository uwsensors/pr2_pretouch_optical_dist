#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include "pr2_pretouch_msgs/GetProbabilisticPointCloud.h"
#include "geometry_msgs/PoseStamped.h"
#include <moveit/move_group_interface/move_group.h>
#include <tf/transform_listener.h>
#include "pr2_pretouch_optical_dist/OpticalRefine.h"
#include <pr2_pretouch_optical_dist/Gripper.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/Eigenvalues>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

geometry_msgs::PoseStamped getPoseFromCloud(sensor_msgs::PointCloud cloud, double tableHeight) {

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcPtr(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcPtr->height = 1;
  pcPtr->width = 0;
  float zMax = cloud.points[0].z;
  float zMin = cloud.points[0].z;

  for(int i = 0; i<cloud.points.size(); i++) {
    pcl::PointXYZRGB point;
    point.x = cloud.points[i].x;
    point.y = cloud.points[i].y;
    point.z = 0.0;//0.01*cloud.points[i].z;
    pcPtr->push_back(point);
    pcPtr->width++;
    if(cloud.points[i].z < zMin) {
      zMin = cloud.points[i].z;
    }else if(cloud.points[i].z > zMax) {
      zMax = cloud.points[ i].z;
    }
//    ROS_INFO("FINISHED POINT %d",i);
  }

  ROS_INFO("AFTER: ZMAX = %f, ZMIN = %f", zMax, zMin);
  ROS_INFO("ABOUT TO GET EIGENVECTORS");
  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*pcPtr, centroid);
  Eigen::Matrix3f covariance;
  pcl::computeCovarianceMatrixNormalized(*pcPtr, centroid, covariance);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigenSolver(covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix3f eigVecs = eigenSolver.eigenvectors();
  eigVecs.col(2) = eigVecs.col(0).cross(eigVecs.col(1));

  ROS_INFO("TRANSFORMING FRAME");
  Eigen::Matrix4f transform(Eigen::Matrix4f::Identity());
  transform.block<3,3>(0,0) = eigVecs.transpose();
  transform.block<3,1>(0,3) =  -1.f * (transform.block<3,3>(0,0) * centroid.head<3>());
  pcl::PointCloud<pcl::PointXYZRGB> cPoints;
  pcl::transformPointCloud(*pcPtr, cPoints, transform);

  pcl::PointXYZRGB minPt, maxPt;
  pcl::getMinMax3D(cPoints, minPt, maxPt);
  const Eigen::Vector3f meanDiag = 0.5f*(maxPt.getVector3fMap() + minPt.getVector3fMap());

  const Eigen::Quaternionf quat(eigVecs);
  const Eigen::Vector3f trans = eigVecs*meanDiag + centroid.head<3>();
	Eigen::Vector4f coeffs = quat.coeffs();
  tf::Quaternion yQuat(0.0, 1.0,0.0, 0.0);
  tf::Quaternion tfQuat(coeffs(0),coeffs(1),coeffs(2),coeffs(3));
//  tf::quaternionEigenToTF(quat, tfQuat);
 
  geometry_msgs::Quaternion finalQuat;
  tfQuat = tfQuat*yQuat;
  double roll, pitch, yaw;
  tf::Matrix3x3(tfQuat).getRPY(roll,pitch,yaw);
//  if(yaw >= M_PI/2.0 || yaw <= -(M_PI/2.0)) {
//	tf::Quaternion tempQuat(0.0, 1.0,0.0,0.0);
//	tfQuat = tfQuat*tempQuat;
//	ROS_INFO("REVERSED QUATERNION");
//  }
  tf::quaternionTFToMsg(tfQuat,finalQuat);

  // Normalize first eigenvector, then scale it by (maxPt.z-minPt.z)/2 + offset from object in gripper's x direction
  Eigen::Vector3f dir = eigVecs.col(2);
  ROS_INFO("First eig vec: x = %f, y = %f, z = %f",eigVecs(0,0),eigVecs(1,0),eigVecs(2,0));
  ROS_INFO("Second eig vec: x = %f, y = %f, z = %f", eigVecs(0,1),eigVecs(1,1), eigVecs(2,1));
  ROS_INFO("Third eig vec: x = %f, y = %f, z = %f", eigVecs(0,2),eigVecs(1,2), eigVecs(2,2));
  dir.normalized();

  dir = dir *((maxPt.z-minPt.z)/2.0+0.22);
  ROS_INFO("Scaled dir: x = %f, y = %f, z = %f",dir(0),dir(1),dir(2));
  geometry_msgs::PoseStamped gripPose;
  gripPose.header.frame_id = "base_link";
  gripPose.header.stamp = ros::Time(0);
  ROS_INFO("trans: x = %f, y = %f, z = %f; dir: x = %f, y = %f, z = %f",trans(0),trans(1), 0.0, dir(0), dir(1), dir(2));
  if(dir(0) < 0) {
    dir(0) = dir(0)*-1;
    dir(1) = dir(1)*-1;
  }
  gripPose.pose.position.x = trans(0);
  gripPose.pose.position.y = trans(1);
  gripPose.pose.position.z = zMax+0.22;//tableHeight + (zMax-zMin)/2.0;
  gripPose.pose.orientation = finalQuat;

	return gripPose;
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "optical_grasp");
	ros::NodeHandle node;

	int optical;
	std::cout << "Enter 1 for optical scanning, 0 for no optical scanning" << std::endl;
	std::cin >> optical;
	if(optical == 1) {
		ROS_ERROR("GOING TO USE OPTICAL SENSOR");
	} else {
		ROS_ERROR("NOT USING OPTICAL SENSOR");
	}

	// Start AsyncSpinner for MoveIt
	ros::AsyncSpinner spinner(1);
	spinner.start();
	Gripper gripper("right");
	gripper.open(-1.0, true);
	moveit::planning_interface::MoveGroup group("right_arm");
//	double temp[7] = {-0.946175, -0.282941, -0.748194, -1.282931, -4.480059, -0.797743, -7.629113};
	double temp[7] = {-0.925614,-0.077037,-1.620044,-1.028569,-18.756835,-0.097424,-17.302886};


	std::vector<double> jointVals(&temp[0], &temp[0]+7);
	group.setJointValueTarget(jointVals);
	group.move();
	spinner.stop(); 
	ros::Duration(2.0).sleep();
/*	moveit::planning_interface::MoveGroup* groupPtr = new moveit::planning_interface::MoveGroup("right_arm");
	std::vector<double> curJointVals = groupPtr->getCurrentJointValues();
	std::vector<std::string> jointNames = groupPtr->getJoints();
	for(int i = 0; i < curJointVals.size(); i++) {
		ROS_ERROR("Name = %s, val = %f", jointNames[i].c_str(), curJointVals[i]);
	} 
	while(ros::ok()); */ 
	// Create services to collect point cloud from kinect and then get it
	ros::ServiceClient requestPCClient = node.serviceClient<std_srvs::Empty>("/tabletop_octomap");
	ros::ServiceClient getPcClient = node.serviceClient<pr2_pretouch_msgs::GetProbabilisticPointCloud>("/get_probabilistic_pointcloud");

	std_srvs::Empty emptySrv;
	ros::service::waitForService("/tabletop_octomap");
	if(requestPCClient.call(emptySrv)) {
		ROS_INFO("tabletop_octomap success");
	} else {
		ROS_INFO("tabletop_octomap failed");
	}

	pr2_pretouch_msgs::GetProbabilisticPointCloud pcSrv;
	ros::service::waitForService("/get_probabilistic_pointcloud");
	if(getPcClient.call(pcSrv)) {
		ROS_INFO("get_probabilistic_pointcloud success");
	} else {
		ROS_INFO("get_probabilistic_pointcloud failed");
	}

	std::string pubTopic1 = "optical_test_original";
	ros::Publisher pubOrigCloud = node.advertise<sensor_msgs::PointCloud>(pubTopic1,1000);

	std::string pubTopic2 = "optical_test";
	ros::Publisher pubCloud = node.advertise<sensor_msgs::PointCloud>(pubTopic2,1000);
	
	pcSrv.response.original_cloud.header.frame_id = "base_link";
	pcSrv.response.cloud.header.frame_id = "base_link";

	ROS_INFO("# of points in cloud = %d", pcSrv.response.cloud.points.size());
	ROS_INFO("# of points in original cloud = %d", pcSrv.response.original_cloud.points.size());
	pubOrigCloud.publish(pcSrv.response.original_cloud);
	pubCloud.publish(pcSrv.response.cloud);

	// Calculate where gripper should go
	geometry_msgs::PoseStamped start;
	start.header.frame_id = "base_link";
	start.header.stamp = ros::Time(0);
	start.pose.position.x = 0.0;
	start.pose.position.y = 0.0;
	start.pose.position.z = 0.0;
	start.pose.orientation.x = 0.0;
	start.pose.orientation.y = 0.0;
	start.pose.orientation.z = 0.0;
	start.pose.orientation.w = 1.0;
//	double avgY = 0.0;
	double minY = pcSrv.response.original_cloud.points[0].y;
	double minX = pcSrv.response.original_cloud.points[0].x;
	double minZ = pcSrv.response.original_cloud.points[0].z;
	for(int i = 0; i < pcSrv.response.original_cloud.points.size(); i++) {
//		avgY += pcSrv.response.original_cloud.points[i].y;
		if(pcSrv.response.original_cloud.points[i].y < minY) {
			minY = pcSrv.response.original_cloud.points[i].y;
		}		
		if(pcSrv.response.original_cloud.points[i].x < minX) {
			minX = pcSrv.response.original_cloud.points[i].x;
		}
		if(pcSrv.response.original_cloud.points[i].z < minZ) {
			minZ = pcSrv.response.original_cloud.points[i].z;
		}
	}
//	avgY = avgY / pcSrv.response.original_cloud.points.size();
//	start.pose.position.x = minX - 0.20;
//	start.pose.position.y = avgY;
	start.pose.position.x = minX + 0.0425;
	if(optical == 1) {
		start.pose.position.y = minY - 0.22;
	} else {
		start.pose.position.y = minY - 0.15;
	}
	
	start.pose.position.z = 0.75;//minZ + 0.1;
	ROS_ERROR("MINZ = %f",minZ);
	tf::Quaternion q;
	q.setRPY(0.0, 0.0, 1.5708);
	geometry_msgs::Quaternion quat;
	tf::quaternionTFToMsg(q, quat);
	start.pose.orientation = quat;

	geometry_msgs::PoseStamped transStart;

//	moveit::planning_interface::MoveGroup group("right_arm");
	tf::TransformListener tfListener;
	tfListener.waitForTransform( "base_link", "r_wrist_roll_link", ros::Time::now(), ros::Duration(1.0));
	try {
		tfListener.transformPose("r_wrist_roll_link",start, transStart);
	} catch(tf::TransformException ex) {
		ROS_ERROR("%s", ex.what());
	}


	ros::ServiceClient moveClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_move");
	pr2_pretouch_optical_dist::OpticalRefine moveSrv;
	transStart.header.frame_id = "r_wrist_roll_link";
	transStart.header.stamp = ros::Time(0);
	moveSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
//	moveSrv.request.goal = transStart;
	moveSrv.request.goal = getPoseFromCloud(pcSrv.response.original_cloud, pcSrv.response.table_tf.translation.z);

	pubOrigCloud.publish(pcSrv.response.original_cloud);
	pubCloud.publish(pcSrv.response.cloud);

	ros::service::waitForService("optical_move");
	ROS_ERROR("ABOUT TO MOVE");
	if(moveClient.call(moveSrv)) {
		ROS_INFO("SUCCESSFUL move");
	} else {
		ROS_INFO("BAD move");
	}
	ROS_ERROR("DONE WITH MOVE");

//	while(ros::ok());
	if(optical == 1) {
		pr2_pretouch_optical_dist::OpticalRefine scanSrv;
		scanSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
		ros::ServiceClient scanClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_horizontal_scan");
		// Use optical scan service
		ros::service::waitForService("optical_horizontal_scan");
		if(scanClient.call(scanSrv)) {
			ROS_INFO("SUCCESSFUL SCAN");
		} else {
			ROS_INFO("BAD SCAN");
		}
		ros::ServiceClient grabClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_detect_grasp");
		// Use optical scan data to grasp
		geometry_msgs::PoseStamped goal;
		goal.header.stamp = ros::Time(0);
		goal.header.frame_id = "r_wrist_roll_link";
		goal.pose.position.x = scanSrv.response.scan[scanSrv.response.scan.size()/2].x + 0.03; // Use median x plus offset to ensure grasp goes far enough
		goal.pose.position.y = (scanSrv.response.scan[0].y + scanSrv.response.scan[scanSrv.response.scan.size()-1].y)/2.0 + 0.00375; // Use average of max and min y val
	//	goal.pose.position.y = srv.response.scan[srv.response.scan.size()/2].y;
		goal.pose.position.z = 0.0;
		goal.pose.orientation.x = 0.0;
		goal.pose.orientation.y = 0.0;
		goal.pose.orientation.z = 0.0;
		goal.pose.orientation.w = 1.0;
		scanSrv.request.goal = goal;

		// Used for trying to reverse grasp
		double x = -(scanSrv.response.scan[scanSrv.response.scan.size()/2].x + 0.03);
		double y = -(goal.pose.position.y + scanSrv.response.scan[scanSrv.response.scan.size()/2].y);
		ros::service::waitForService("optical_detect_grasp");
		if(grabClient.call(scanSrv)) {
			ROS_INFO("SUCCESSFUL Detect");
		} else {
			ROS_INFO("BAD Detect");
		}
		ROS_ERROR("RETURNED FROM ALL SERVICES");
	}


	
	gripper.close(25.0,false);

	transStart.pose.position.x = 0.0;
	transStart.pose.position.y = 0.0;
	transStart.pose.position.z = 0.01;
	transStart.pose.orientation.x = 0.0;
	transStart.pose.orientation.y = 0.0;	
	transStart.pose.orientation.z = 0.0;
	transStart.pose.orientation.w = 1.0;
	transStart.header.frame_id = "r_wrist_roll_link";
	transStart.header.stamp = ros::Time(0);
	moveSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
	moveSrv.request.goal = transStart;

	ros::service::waitForService("optical_move");
	ROS_ERROR("ABOUT TO MOVE");
	if(moveClient.call(moveSrv)) {
		ROS_INFO("SUCCESSFUL move");
	} else {
		ROS_INFO("BAD move");
	}
	ROS_ERROR("DONE WITH MOVE");

	std::string release;
	std::cin >> release;
	
	gripper.open();	
	spinner.start();
	group.setJointValueTarget(jointVals);
	group.move();

	while(ros::ok()) {
		pubOrigCloud.publish(pcSrv.response.original_cloud);
		pubCloud.publish(pcSrv.response.cloud);
		ros::Duration(1.0).sleep();
	}

	return 0;
}


