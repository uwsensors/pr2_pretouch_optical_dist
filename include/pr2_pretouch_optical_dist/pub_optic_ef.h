#ifndef PUB_OPTIC_EF_H
#define PUB_OPTIC_EF_H

#include "ros/ros.h"
#include "std_msgs/ByteMultiArray.h"
#include "pr2_pretouch_optical_dist/OpticalDist.h"
#include "pr2_pretouch_optical_dist/EField.h"

#include <vector>

class PubOpticEF {

public:
	PubOpticEF(ros::NodeHandle& node, std::string& side);
	~PubOpticEF();
	void dataCallback(const std_msgs::ByteMultiArray& msg);

private:
	std::string optic_pub_topic_;
	std::string ef_pub_topic_;
  	std::string sub_topic_;
	ros::Publisher optic_pub_;
	ros::Publisher ef_pub_;
	ros::Subscriber sub_;
	std::vector<int> ts_;
	pr2_pretouch_optical_dist::OpticalDist optic_data_;
	pr2_pretouch_optical_dist::EField ef_data_; 
	int seq_;
};

#endif
