#include "ros/ros.h"
#include "pr2_pretouch_optical_dist/OpticalRefine.h"
#include <moveit/move_group_interface/move_group.h>

int main(int argc, char** argv) {

	ros::init(argc, argv, "optical_refine_client");
	ros::NodeHandle node;

	// Create services to scan, grasp, and detect items
	ros::ServiceClient scanClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_scan");
	ros::ServiceClient grabClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_detect_grasp");
	ros::ServiceClient itemClient = node.serviceClient<pr2_pretouch_optical_dist::OpticalRefine>("optical_detect_item");

	// Create a service and specify right arm
	pr2_pretouch_optical_dist::OpticalRefine srv;
	srv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
/***************************** Code to use item detection service ***********************************/
/*
	while(1) {
		pr2_pretouch_optical_dist::OpticalRefine itemSrv;
		itemSrv.request.arm = pr2_pretouch_optical_dist::OpticalRefineRequest::RIGHT_ARM;
		ros::service::waitForService("optical_detect_item");
		itemClient.call(itemSrv);
		if(itemSrv.response.detect) {
			ROS_ERROR("ITEM DETECTED");
		} else {
			ROS_ERROR("NO ITEM DETECTED");
		}
		ros::Duration(1.0).sleep();
	} */
/***************************************************************************************************/

	// Use optical scan service
	ros::service::waitForService("optical_scan");
	ROS_ERROR("ABOUT TO CALL SCAN CLIENT");
	if(scanClient.call(srv)) {
		ROS_INFO("SUCCESSFUL SCAN");
	} else {
		ROS_INFO("BAD SCAN");
	}

	// Use optical scan data to grasp
	geometry_msgs::PoseStamped goal;
	goal.header.stamp = ros::Time(0);
	goal.header.frame_id = "r_wrist_roll_link";
	goal.pose.position.x = srv.response.scan[srv.response.scan.size()/2].x + 0.03; // Use median x plus offset to ensure grasp goes far enough
	goal.pose.position.y = (srv.response.scan[0].y + srv.response.scan[srv.response.scan.size()-1].y)/2.0; // Use average of max and min y val
//	goal.pose.position.y = srv.response.scan[srv.response.scan.size()/2].y;
	goal.pose.position.z = 0.0;
	goal.pose.orientation.x = 0.0;
	goal.pose.orientation.y = 0.0;
	goal.pose.orientation.z = 0.0;
	goal.pose.orientation.w = 1.0;
	srv.request.goal = goal;

	// Used for trying to reverse grasp
	double x = -(srv.response.scan[srv.response.scan.size()/2].x + 0.03);
	double y = -(goal.pose.position.y = srv.response.scan[srv.response.scan.size()/2].y);
	ros::service::waitForService("optical_detect_grasp");
	if(grabClient.call(srv)) {
		ROS_INFO("SUCCESSFUL Detect");
	} else {
		ROS_INFO("BAD Detect");
	}
	ROS_ERROR("RETURNED FROM ALL SERVICES");

	// Move back
	goal.pose.position.x = x;
	goal.pose.position.y = y;
	srv.request.goal = goal;
	if(grabClient.call(srv)) {
		ROS_INFO("SUCCESSFUL Detect");
	} else {
		ROS_INFO("BAD Detect");
	}
	ROS_ERROR("RETURNED FROM ALL SERVICES");
	
	return 0;

}
