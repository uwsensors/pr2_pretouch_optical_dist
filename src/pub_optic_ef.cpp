#include "pr2_pretouch_optical_dist/pub_optic_ef.h"

PubOpticEF::PubOpticEF(ros::NodeHandle& node, std::string& side) : optic_pub_topic_("/optical/"+side), ef_pub_topic_("/efield/"+side), sub_topic_("/raw_pressure/"+side.substr(0,1)+"_gripper_motor"), optic_pub_(node.advertise<pr2_pretouch_optical_dist::OpticalDist>(optic_pub_topic_,1000)), ef_pub_(node.advertise<pr2_pretouch_optical_dist::EField>(ef_pub_topic_, 1000)), sub_(node.subscribe(sub_topic_, 1000, &PubOpticEF::dataCallback, this)), ts_(4,0), seq_(0) {

}

PubOpticEF::~PubOpticEF() {

}

void PubOpticEF::dataCallback(const std_msgs::ByteMultiArray& msg) {

	if(msg.data[0] == ts_[0] && msg.data[1] == ts_[1] && msg.data[2] == ts_[2] && msg.data[3] == ts_[3]) {
		return;
	}

	// Set timestamp
	ts_[0] = msg.data[0];
	ts_[1] = msg.data[1];
	ts_[2] = msg.data[2];
	ts_[3] = msg.data[3];

	ros::Time now = ros::Time::now();
	optic_data_.header.seq = seq_;
	optic_data_.header.stamp = now;
	ef_data_.header.seq = seq_;
	ef_data_.header.stamp = now;

	// msg.data[4] is junk
	int16_t inphase = 0x0;
	int16_t quad = 0x0;
	
	optic_data_.centerData.resize(1);
	optic_data_.centerData[0] = msg.data[6] > 0 ? msg.data[6] : msg.data[6]+256;

	optic_data_.leftBackLateralData.resize(1);	
	optic_data_.leftBackLateralData[0] = msg.data[7] > 0 ? msg.data[7] : msg.data[7]+256;

	optic_data_.leftFrontLateralData.resize(1);
	optic_data_.leftFrontLateralData[0] = msg.data[8] > 0 ? msg.data[8] : msg.data[8]+256;

	optic_data_.frontTipData.resize(1);
	optic_data_.frontTipData[0] = msg.data[9] > 0 ? msg.data[9] : msg.data[9]+256;

	optic_data_.rightBackLateralData.resize(1);
	optic_data_.rightBackLateralData[0] = msg.data[10] > 0 ? msg.data[10] : msg.data[10]+256;

	optic_data_.rightFrontLateralData.resize(1);
	optic_data_.rightFrontLateralData[0] = msg.data[11] > 0 ? msg.data[11] : msg.data[11]+256;
	optic_pub_.publish(optic_data_);

	int offset = msg.data[16] == 0 ? 0 : 1; // HACK - sometimes dummy data shows up in msg.data[12], sometimes it doesn't
	for(int i = 0; i < 2; i++) {
		inphase |= (0x00FF&msg.data[i+offset+12]) << (8*i);
		quad |= (0x00FF&msg.data[i+offset+14]) << (8*i);
	}

	double mag = std::sqrt(std::pow(inphase,2)+std::pow(quad,2));
	double phase = std::atan2(inphase, quad);
	ef_data_.lData.resize(4);
	ef_data_.lData[0] = inphase;
	ef_data_.lData[1] = quad;
	ef_data_.lData[2] = mag;
	ef_data_.lData[3] = phase;

	ef_pub_.publish(ef_data_);
	
//	ROS_INFO("%d %d %d %d %d %d %d %d %d",msg.data[4], msg.data[5], msg.data[6], msg.data[7], msg.data[8], msg.data[9], msg.data[10], msg.data[11], msg.data[12]);

	seq_++;
}

int main(int argc, char** argv){

	ros::init(argc, argv, "optic_ef_pub");
	ros::NodeHandle node;

	std::string side("right");
	
	if(argc >= 2) {
		side = argv[1];
	}

	PubOpticEF optic_ef_pub(node,side);
	ROS_INFO("Started publisher for the %s gripper", side.c_str());
	ros::spin();
}
