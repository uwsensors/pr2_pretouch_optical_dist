#ifndef OPTICAL_REFINE_SERVICE_H
#define OPTICAL_REFINE_SERVICE_H

#include "pr2_pretouch_optical_dist/OpticalScanner.h"
#include "pr2_pretouch_optical_dist/OpticalRefine.h"

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

class OpticalRefine{

	private: ros::NodeHandle* nodePtr;
		 ros::ServiceServer serviceHorizontalScan;
		 ros::ServiceServer serviceEdgeScan;
		 ros::ServiceServer serviceDetectGrasp;
		 ros::ServiceServer serviceDetectItem;
		 ros::ServiceServer serviceMove;
		 OpticalScanner* rOptScanner;
		 OpticalScanner* lOptScanner;

	public: OpticalRefine(ros::NodeHandle* nodePtr);
		~OpticalRefine();
		// Preforms a scan of the object
		bool horizontalScan(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response);
		// Moves to the goal until something is detected between the fingertips
		bool graspDetect(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response);
		// Detects if an item is blocking one of the sensors (not including the center one on the pad)
		bool itemDetect(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response);

		bool waypointsMove(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response);
		bool edgeScan(pr2_pretouch_optical_dist::OpticalRefine::Request & request, pr2_pretouch_optical_dist::OpticalRefine::Response &response);
};

#endif
